<?php

return [
//    'gcm' => [
//        'priority' => 'normal',
//        'dry_run'  => false,
//        'apiKey'   => 'My_ApiKey',
//    ],
    'fcm' => [
        'priority' => 'high',
        'dry_run'  => false,
        //server key from firebase to make this work
        'apiKey'   => 'AAAADZ-JIHw:APA91bHwuEnHsWTMqrwLrh0a-IxbZ1qGgeBiS14Sp8BlhGhxAp4am41Gzo57xb79nXHo9WT_ooeX4yYDe1FZXf9h67gH53YzUX2o5wAwMQfqTad6VelK4mnALKoFtiaoeAoxuwGtrH2b',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase'  => '1234', //Optional
        'passFile'    => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run'     => true
    ]
];