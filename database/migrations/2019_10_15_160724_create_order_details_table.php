<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->increments('order_id');
            $table->increments('no_bags');
            $table->boolean('type');
            $table->boolean('detergent');
            $table->boolean('folded');
            $table->boolean('hung');
            $table->text('instruction', 65535);
            $table->string('price', 255);
            $table->string('donation', 255);
            $table->datetime('deleted_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_details');
    }
}
