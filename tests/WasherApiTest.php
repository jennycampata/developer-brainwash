<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WasherApiTest extends TestCase
{
    use MakeWasherTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateWasher()
    {
        $washer = $this->fakeWasherData();
        $this->json('POST', '/api/v1/washers', $washer);

        $this->assertApiResponse($washer);
    }

    /**
     * @test
     */
    public function testReadWasher()
    {
        $washer = $this->makeWasher();
        $this->json('GET', '/api/v1/washers/'.$washer->id);

        $this->assertApiResponse($washer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateWasher()
    {
        $washer = $this->makeWasher();
        $editedWasher = $this->fakeWasherData();

        $this->json('PUT', '/api/v1/washers/'.$washer->id, $editedWasher);

        $this->assertApiResponse($editedWasher);
    }

    /**
     * @test
     */
    public function testDeleteWasher()
    {
        $washer = $this->makeWasher();
        $this->json('DELETE', '/api/v1/washers/'.$washer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/washers/'.$washer->id);

        $this->assertResponseStatus(404);
    }
}
