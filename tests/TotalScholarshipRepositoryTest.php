<?php

use App\Models\TotalScholarship;
use App\Repositories\Admin\TotalScholarshipRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TotalScholarshipRepositoryTest extends TestCase
{
    use MakeTotalScholarshipTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TotalScholarshipRepository
     */
    protected $totalScholarshipRepo;

    public function setUp()
    {
        parent::setUp();
        $this->totalScholarshipRepo = App::make(TotalScholarshipRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTotalScholarship()
    {
        $totalScholarship = $this->fakeTotalScholarshipData();
        $createdTotalScholarship = $this->totalScholarshipRepo->create($totalScholarship);
        $createdTotalScholarship = $createdTotalScholarship->toArray();
        $this->assertArrayHasKey('id', $createdTotalScholarship);
        $this->assertNotNull($createdTotalScholarship['id'], 'Created TotalScholarship must have id specified');
        $this->assertNotNull(TotalScholarship::find($createdTotalScholarship['id']), 'TotalScholarship with given id must be in DB');
        $this->assertModelData($totalScholarship, $createdTotalScholarship);
    }

    /**
     * @test read
     */
    public function testReadTotalScholarship()
    {
        $totalScholarship = $this->makeTotalScholarship();
        $dbTotalScholarship = $this->totalScholarshipRepo->find($totalScholarship->id);
        $dbTotalScholarship = $dbTotalScholarship->toArray();
        $this->assertModelData($totalScholarship->toArray(), $dbTotalScholarship);
    }

    /**
     * @test update
     */
    public function testUpdateTotalScholarship()
    {
        $totalScholarship = $this->makeTotalScholarship();
        $fakeTotalScholarship = $this->fakeTotalScholarshipData();
        $updatedTotalScholarship = $this->totalScholarshipRepo->update($fakeTotalScholarship, $totalScholarship->id);
        $this->assertModelData($fakeTotalScholarship, $updatedTotalScholarship->toArray());
        $dbTotalScholarship = $this->totalScholarshipRepo->find($totalScholarship->id);
        $this->assertModelData($fakeTotalScholarship, $dbTotalScholarship->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTotalScholarship()
    {
        $totalScholarship = $this->makeTotalScholarship();
        $resp = $this->totalScholarshipRepo->delete($totalScholarship->id);
        $this->assertTrue($resp);
        $this->assertNull(TotalScholarship::find($totalScholarship->id), 'TotalScholarship should not exist in DB');
    }
}
