<?php

use App\Models\Washer;
use App\Repositories\Admin\WasherRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WasherRepositoryTest extends TestCase
{
    use MakeWasherTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var WasherRepository
     */
    protected $washerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->washerRepo = App::make(WasherRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateWasher()
    {
        $washer = $this->fakeWasherData();
        $createdWasher = $this->washerRepo->create($washer);
        $createdWasher = $createdWasher->toArray();
        $this->assertArrayHasKey('id', $createdWasher);
        $this->assertNotNull($createdWasher['id'], 'Created Washer must have id specified');
        $this->assertNotNull(Washer::find($createdWasher['id']), 'Washer with given id must be in DB');
        $this->assertModelData($washer, $createdWasher);
    }

    /**
     * @test read
     */
    public function testReadWasher()
    {
        $washer = $this->makeWasher();
        $dbWasher = $this->washerRepo->find($washer->id);
        $dbWasher = $dbWasher->toArray();
        $this->assertModelData($washer->toArray(), $dbWasher);
    }

    /**
     * @test update
     */
    public function testUpdateWasher()
    {
        $washer = $this->makeWasher();
        $fakeWasher = $this->fakeWasherData();
        $updatedWasher = $this->washerRepo->update($fakeWasher, $washer->id);
        $this->assertModelData($fakeWasher, $updatedWasher->toArray());
        $dbWasher = $this->washerRepo->find($washer->id);
        $this->assertModelData($fakeWasher, $dbWasher->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteWasher()
    {
        $washer = $this->makeWasher();
        $resp = $this->washerRepo->delete($washer->id);
        $this->assertTrue($resp);
        $this->assertNull(Washer::find($washer->id), 'Washer should not exist in DB');
    }
}
