<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TotalScholarshipApiTest extends TestCase
{
    use MakeTotalScholarshipTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateTotalScholarship()
    {
        $totalScholarship = $this->fakeTotalScholarshipData();
        $this->json('POST', '/api/v1/totalScholarships', $totalScholarship);

        $this->assertApiResponse($totalScholarship);
    }

    /**
     * @test
     */
    public function testReadTotalScholarship()
    {
        $totalScholarship = $this->makeTotalScholarship();
        $this->json('GET', '/api/v1/totalScholarships/'.$totalScholarship->id);

        $this->assertApiResponse($totalScholarship->toArray());
    }

    /**
     * @test
     */
    public function testUpdateTotalScholarship()
    {
        $totalScholarship = $this->makeTotalScholarship();
        $editedTotalScholarship = $this->fakeTotalScholarshipData();

        $this->json('PUT', '/api/v1/totalScholarships/'.$totalScholarship->id, $editedTotalScholarship);

        $this->assertApiResponse($editedTotalScholarship);
    }

    /**
     * @test
     */
    public function testDeleteTotalScholarship()
    {
        $totalScholarship = $this->makeTotalScholarship();
        $this->json('DELETE', '/api/v1/totalScholarships/'.$totalScholarship->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/totalScholarships/'.$totalScholarship->id);

        $this->assertResponseStatus(404);
    }
}
