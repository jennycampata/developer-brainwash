<?php

use Faker\Factory as Faker;
use App\Models\Job;
use App\Repositories\Admin\JobRepository;

trait MakeJobTrait
{
    /**
     * Create fake instance of Job and save it in database
     *
     * @param array $jobFields
     * @return Job
     */
    public function makeJob($jobFields = [])
    {
        /** @var JobRepository $jobRepo */
        $jobRepo = App::make(JobRepository::class);
        $theme = $this->fakeJobData($jobFields);
        return $jobRepo->create($theme);
    }

    /**
     * Get fake instance of Job
     *
     * @param array $jobFields
     * @return Job
     */
    public function fakeJob($jobFields = [])
    {
        return new Job($this->fakeJobData($jobFields));
    }

    /**
     * Get fake data of Job
     *
     * @param array $postFields
     * @return array
     */
    public function fakeJobData($jobFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->word,
            'order_id' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $jobFields);
    }
}
