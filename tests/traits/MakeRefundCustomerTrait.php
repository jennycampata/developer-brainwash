<?php

use Faker\Factory as Faker;
use App\Models\RefundCustomer;
use App\Repositories\Admin\RefundCustomerRepository;

trait MakeRefundCustomerTrait
{
    /**
     * Create fake instance of RefundCustomer and save it in database
     *
     * @param array $refundCustomerFields
     * @return RefundCustomer
     */
    public function makeRefundCustomer($refundCustomerFields = [])
    {
        /** @var RefundCustomerRepository $refundCustomerRepo */
        $refundCustomerRepo = App::make(RefundCustomerRepository::class);
        $theme = $this->fakeRefundCustomerData($refundCustomerFields);
        return $refundCustomerRepo->create($theme);
    }

    /**
     * Get fake instance of RefundCustomer
     *
     * @param array $refundCustomerFields
     * @return RefundCustomer
     */
    public function fakeRefundCustomer($refundCustomerFields = [])
    {
        return new RefundCustomer($this->fakeRefundCustomerData($refundCustomerFields));
    }

    /**
     * Get fake data of RefundCustomer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRefundCustomerData($refundCustomerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->word,
            'university_id' => $fake->word,
            'amount' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $refundCustomerFields);
    }
}
