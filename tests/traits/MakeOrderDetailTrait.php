<?php

use Faker\Factory as Faker;
use App\Models\OrderDetail;
use App\Repositories\Admin\OrderDetailRepository;

trait MakeOrderDetailTrait
{
    /**
     * Create fake instance of OrderDetail and save it in database
     *
     * @param array $orderDetailFields
     * @return OrderDetail
     */
    public function makeOrderDetail($orderDetailFields = [])
    {
        /** @var OrderDetailRepository $orderDetailRepo */
        $orderDetailRepo = App::make(OrderDetailRepository::class);
        $theme = $this->fakeOrderDetailData($orderDetailFields);
        return $orderDetailRepo->create($theme);
    }

    /**
     * Get fake instance of OrderDetail
     *
     * @param array $orderDetailFields
     * @return OrderDetail
     */
    public function fakeOrderDetail($orderDetailFields = [])
    {
        return new OrderDetail($this->fakeOrderDetailData($orderDetailFields));
    }

    /**
     * Get fake data of OrderDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeOrderDetailData($orderDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->word,
            'no_bags' => $fake->word,
            'type' => $fake->word,
            'detergent' => $fake->word,
            'folded' => $fake->word,
            'hung' => $fake->word,
            'instruction' => $fake->text,
            'price' => $fake->word,
            'donation' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $orderDetailFields);
    }
}
