<?php

use Faker\Factory as Faker;
use App\Models\TotalScholarship;
use App\Repositories\Admin\TotalScholarshipRepository;

trait MakeTotalScholarshipTrait
{
    /**
     * Create fake instance of TotalScholarship and save it in database
     *
     * @param array $totalScholarshipFields
     * @return TotalScholarship
     */
    public function makeTotalScholarship($totalScholarshipFields = [])
    {
        /** @var TotalScholarshipRepository $totalScholarshipRepo */
        $totalScholarshipRepo = App::make(TotalScholarshipRepository::class);
        $theme = $this->fakeTotalScholarshipData($totalScholarshipFields);
        return $totalScholarshipRepo->create($theme);
    }

    /**
     * Get fake instance of TotalScholarship
     *
     * @param array $totalScholarshipFields
     * @return TotalScholarship
     */
    public function fakeTotalScholarship($totalScholarshipFields = [])
    {
        return new TotalScholarship($this->fakeTotalScholarshipData($totalScholarshipFields));
    }

    /**
     * Get fake data of TotalScholarship
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTotalScholarshipData($totalScholarshipFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'university_id' => $fake->word,
            'total_raised' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $totalScholarshipFields);
    }
}
