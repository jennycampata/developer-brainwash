<?php

use Faker\Factory as Faker;
use App\Models\University;
use App\Repositories\Admin\UniversityRepository;

trait MakeUniversityTrait
{
    /**
     * Create fake instance of University and save it in database
     *
     * @param array $universityFields
     * @return University
     */
    public function makeUniversity($universityFields = [])
    {
        /** @var UniversityRepository $universityRepo */
        $universityRepo = App::make(UniversityRepository::class);
        $theme = $this->fakeUniversityData($universityFields);
        return $universityRepo->create($theme);
    }

    /**
     * Get fake instance of University
     *
     * @param array $universityFields
     * @return University
     */
    public function fakeUniversity($universityFields = [])
    {
        return new University($this->fakeUniversityData($universityFields));
    }

    /**
     * Get fake data of University
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUniversityData($universityFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $universityFields);
    }
}
