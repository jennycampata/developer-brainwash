<?php

use Faker\Factory as Faker;
use App\Models\Washer;
use App\Repositories\Admin\WasherRepository;

trait MakeWasherTrait
{
    /**
     * Create fake instance of Washer and save it in database
     *
     * @param array $washerFields
     * @return Washer
     */
    public function makeWasher($washerFields = [])
    {
        /** @var WasherRepository $washerRepo */
        $washerRepo = App::make(WasherRepository::class);
        $theme = $this->fakeWasherData($washerFields);
        return $washerRepo->create($theme);
    }

    /**
     * Get fake instance of Washer
     *
     * @param array $washerFields
     * @return Washer
     */
    public function fakeWasher($washerFields = [])
    {
        return new Washer($this->fakeWasherData($washerFields));
    }

    /**
     * Get fake data of Washer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeWasherData($washerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'email' => $fake->word,
            'password' => $fake->word,
            'remember_token' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $washerFields);
    }
}
