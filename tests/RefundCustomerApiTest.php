<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RefundCustomerApiTest extends TestCase
{
    use MakeRefundCustomerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRefundCustomer()
    {
        $refundCustomer = $this->fakeRefundCustomerData();
        $this->json('POST', '/api/v1/refundCustomers', $refundCustomer);

        $this->assertApiResponse($refundCustomer);
    }

    /**
     * @test
     */
    public function testReadRefundCustomer()
    {
        $refundCustomer = $this->makeRefundCustomer();
        $this->json('GET', '/api/v1/refundCustomers/'.$refundCustomer->id);

        $this->assertApiResponse($refundCustomer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRefundCustomer()
    {
        $refundCustomer = $this->makeRefundCustomer();
        $editedRefundCustomer = $this->fakeRefundCustomerData();

        $this->json('PUT', '/api/v1/refundCustomers/'.$refundCustomer->id, $editedRefundCustomer);

        $this->assertApiResponse($editedRefundCustomer);
    }

    /**
     * @test
     */
    public function testDeleteRefundCustomer()
    {
        $refundCustomer = $this->makeRefundCustomer();
        $this->json('DELETE', '/api/v1/refundCustomers/'.$refundCustomer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/refundCustomers/'.$refundCustomer->id);

        $this->assertResponseStatus(404);
    }
}
