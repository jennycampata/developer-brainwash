<?php

use App\Models\RefundCustomer;
use App\Repositories\Admin\RefundCustomerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RefundCustomerRepositoryTest extends TestCase
{
    use MakeRefundCustomerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RefundCustomerRepository
     */
    protected $refundCustomerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->refundCustomerRepo = App::make(RefundCustomerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRefundCustomer()
    {
        $refundCustomer = $this->fakeRefundCustomerData();
        $createdRefundCustomer = $this->refundCustomerRepo->create($refundCustomer);
        $createdRefundCustomer = $createdRefundCustomer->toArray();
        $this->assertArrayHasKey('id', $createdRefundCustomer);
        $this->assertNotNull($createdRefundCustomer['id'], 'Created RefundCustomer must have id specified');
        $this->assertNotNull(RefundCustomer::find($createdRefundCustomer['id']), 'RefundCustomer with given id must be in DB');
        $this->assertModelData($refundCustomer, $createdRefundCustomer);
    }

    /**
     * @test read
     */
    public function testReadRefundCustomer()
    {
        $refundCustomer = $this->makeRefundCustomer();
        $dbRefundCustomer = $this->refundCustomerRepo->find($refundCustomer->id);
        $dbRefundCustomer = $dbRefundCustomer->toArray();
        $this->assertModelData($refundCustomer->toArray(), $dbRefundCustomer);
    }

    /**
     * @test update
     */
    public function testUpdateRefundCustomer()
    {
        $refundCustomer = $this->makeRefundCustomer();
        $fakeRefundCustomer = $this->fakeRefundCustomerData();
        $updatedRefundCustomer = $this->refundCustomerRepo->update($fakeRefundCustomer, $refundCustomer->id);
        $this->assertModelData($fakeRefundCustomer, $updatedRefundCustomer->toArray());
        $dbRefundCustomer = $this->refundCustomerRepo->find($refundCustomer->id);
        $this->assertModelData($fakeRefundCustomer, $dbRefundCustomer->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRefundCustomer()
    {
        $refundCustomer = $this->makeRefundCustomer();
        $resp = $this->refundCustomerRepo->delete($refundCustomer->id);
        $this->assertTrue($resp);
        $this->assertNull(RefundCustomer::find($refundCustomer->id), 'RefundCustomer should not exist in DB');
    }
}
