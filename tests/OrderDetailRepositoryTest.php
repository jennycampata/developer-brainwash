<?php

use App\Models\OrderDetail;
use App\Repositories\Admin\OrderDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderDetailRepositoryTest extends TestCase
{
    use MakeOrderDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var OrderDetailRepository
     */
    protected $orderDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->orderDetailRepo = App::make(OrderDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateOrderDetail()
    {
        $orderDetail = $this->fakeOrderDetailData();
        $createdOrderDetail = $this->orderDetailRepo->create($orderDetail);
        $createdOrderDetail = $createdOrderDetail->toArray();
        $this->assertArrayHasKey('id', $createdOrderDetail);
        $this->assertNotNull($createdOrderDetail['id'], 'Created OrderDetail must have id specified');
        $this->assertNotNull(OrderDetail::find($createdOrderDetail['id']), 'OrderDetail with given id must be in DB');
        $this->assertModelData($orderDetail, $createdOrderDetail);
    }

    /**
     * @test read
     */
    public function testReadOrderDetail()
    {
        $orderDetail = $this->makeOrderDetail();
        $dbOrderDetail = $this->orderDetailRepo->find($orderDetail->id);
        $dbOrderDetail = $dbOrderDetail->toArray();
        $this->assertModelData($orderDetail->toArray(), $dbOrderDetail);
    }

    /**
     * @test update
     */
    public function testUpdateOrderDetail()
    {
        $orderDetail = $this->makeOrderDetail();
        $fakeOrderDetail = $this->fakeOrderDetailData();
        $updatedOrderDetail = $this->orderDetailRepo->update($fakeOrderDetail, $orderDetail->id);
        $this->assertModelData($fakeOrderDetail, $updatedOrderDetail->toArray());
        $dbOrderDetail = $this->orderDetailRepo->find($orderDetail->id);
        $this->assertModelData($fakeOrderDetail, $dbOrderDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteOrderDetail()
    {
        $orderDetail = $this->makeOrderDetail();
        $resp = $this->orderDetailRepo->delete($orderDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(OrderDetail::find($orderDetail->id), 'OrderDetail should not exist in DB');
    }
}
