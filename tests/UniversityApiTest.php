<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UniversityApiTest extends TestCase
{
    use MakeUniversityTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUniversity()
    {
        $university = $this->fakeUniversityData();
        $this->json('POST', '/api/v1/universities', $university);

        $this->assertApiResponse($university);
    }

    /**
     * @test
     */
    public function testReadUniversity()
    {
        $university = $this->makeUniversity();
        $this->json('GET', '/api/v1/universities/'.$university->id);

        $this->assertApiResponse($university->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUniversity()
    {
        $university = $this->makeUniversity();
        $editedUniversity = $this->fakeUniversityData();

        $this->json('PUT', '/api/v1/universities/'.$university->id, $editedUniversity);

        $this->assertApiResponse($editedUniversity);
    }

    /**
     * @test
     */
    public function testDeleteUniversity()
    {
        $university = $this->makeUniversity();
        $this->json('DELETE', '/api/v1/universities/'.$university->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/universities/'.$university->id);

        $this->assertResponseStatus(404);
    }
}
