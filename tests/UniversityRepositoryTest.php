<?php

use App\Models\University;
use App\Repositories\Admin\UniversityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UniversityRepositoryTest extends TestCase
{
    use MakeUniversityTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UniversityRepository
     */
    protected $universityRepo;

    public function setUp()
    {
        parent::setUp();
        $this->universityRepo = App::make(UniversityRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUniversity()
    {
        $university = $this->fakeUniversityData();
        $createdUniversity = $this->universityRepo->create($university);
        $createdUniversity = $createdUniversity->toArray();
        $this->assertArrayHasKey('id', $createdUniversity);
        $this->assertNotNull($createdUniversity['id'], 'Created University must have id specified');
        $this->assertNotNull(University::find($createdUniversity['id']), 'University with given id must be in DB');
        $this->assertModelData($university, $createdUniversity);
    }

    /**
     * @test read
     */
    public function testReadUniversity()
    {
        $university = $this->makeUniversity();
        $dbUniversity = $this->universityRepo->find($university->id);
        $dbUniversity = $dbUniversity->toArray();
        $this->assertModelData($university->toArray(), $dbUniversity);
    }

    /**
     * @test update
     */
    public function testUpdateUniversity()
    {
        $university = $this->makeUniversity();
        $fakeUniversity = $this->fakeUniversityData();
        $updatedUniversity = $this->universityRepo->update($fakeUniversity, $university->id);
        $this->assertModelData($fakeUniversity, $updatedUniversity->toArray());
        $dbUniversity = $this->universityRepo->find($university->id);
        $this->assertModelData($fakeUniversity, $dbUniversity->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUniversity()
    {
        $university = $this->makeUniversity();
        $resp = $this->universityRepo->delete($university->id);
        $this->assertTrue($resp);
        $this->assertNull(University::find($university->id), 'University should not exist in DB');
    }
}
