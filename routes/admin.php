<?php


Auth::routes();

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('dashboard');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('about', 'HomeController@index');


Route::resource('roles', 'RoleController');

Route::resource('modules', 'ModuleController');

Route::get('/module/step1/{id?}', 'ModuleController@getStep1')->name('modules.create');
Route::get('/module/step2/{tablename?}', 'ModuleController@getStep2')->name('modules.create');
Route::get('/getJoinFields/{tablename?}', 'ModuleController@getJoinFields');
Route::get('/module/step3/{tablename?}', 'ModuleController@getStep3')->name('modules.create');

Route::post('/step1', 'ModuleController@postStep1');
Route::post('/step2', 'ModuleController@postStep2');
Route::post('/step3', 'ModuleController@postStep3');


Route::resource('users', 'UserController');

Route::resource('permissions', 'PermissionController');

//Route::resource('profile', 'UserController');

Route::get('user/profile', 'UserController@profile')->name('users.profile');
//Route::patch('users/profile-update/{id}', 'UserController@profileUpdate')->name('users.profile-update');

Route::resource('languages', 'LanguageController');

Route::resource('pages', 'PageController');

Route::resource('contactus', 'ContactUsController');

Route::resource('notifications', 'NotificationController');

Route::resource('menus', 'MenuController');

//Menu #TODO need to be fixed
Route::get('statusChange/{id}', 'MenuController@statusChange');

Route::post('updateChannelPosition', 'MenuController@update_channel_position')->name('channels');

Route::resource('settings', 'SettingController');

Route::resource('order-details', 'OrderDetailController');

//added this
Route::post('/universities/toggleBtn', 'UniversityController@toggleBtn');

Route::post('/customers/toggleBtn', 'CustomerController@toggleBtn');

Route::post('/jobs/toggleBtn', 'JobController@toggleBtn');

Route::post('/transactions/toggleBtn', 'TransactionController@toggleBtn');

Route::post('/refund-customers/refund', 'RefundCustomerController@refund');

Route::post('/refund-customers/toggleBtn', 'RefundCustomerController@toggleBtn');

Route::resource('customers', 'CustomerController');

Route::resource('washers', 'WasherController');

Route::resource('universities', 'UniversityController');

Route::resource('transactions', 'TransactionController');

Route::resource('orders', 'OrderController');

Route::resource('transactions', 'TransactionController');

Route::resource('universities', 'UniversityController');

Route::resource('reports', 'ReportController');

Route::resource('jobs', 'JobController');

Route::resource('total-scholarships', 'TotalScholarshipController');

Route::resource('refund-customers', 'RefundCustomerController');

Route::resource('cancelled-orders', 'CancelledOrderController');

Route::resource('cancelled-orders', 'CancelledOrderController');

Route::resource('cancelled-orders', 'CancelledOrderController');

Route::resource('wallets', 'WalletController');