<?php

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Images Resize Route
Route::get('/resize/{img}', function ($img) {

    ob_end_clean();
    try {
        $w = request()->get('w');
        $h = request()->get('h');
        $crop = request()->get('crop', false);
        $method = ($crop) ? "fit" : "resize";
        if ($h && $w) {
            // Image Handler lib
            return Image::make(asset("storage/app/$img"))->$method($w, $h, function ($c) {
                $c->upsize();
                $c->aspectRatio();
            })->response('png');
        } else {
            return response(file_get_contents(storage_path("/app/$img")))
                ->header('Content-Type', 'image/png');
        }

    } catch (\Exception $e) {
//        dd($e->getMessage());
        return abort(404, $e->getMessage());
    }
})->name('resize')->where('img', '(.*)');


/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


if (is_object(JWTAuth::getToken())) {
    ## Token Required to below APIs

    Route::middleware('auth:api')->group(function () {
        ## Token Required to below APIs

        Route::post('v1/logout', 'AuthAPIController@logout');

        Route::post('v1/change-password', 'AuthAPIController@changePassword');

        Route::post('v1/refresh', 'AuthAPIController@refresh');

        Route::post('v1/me', 'AuthAPIController@me');

        Route::resource('v1/users', 'UserAPIController');

        Route::resource('v1/transactions', 'TransactionAPIController');

        Route::post('v1/userupdate', 'UserAPIController@updateProfile')->name('updateProfile');

        Route::post('v1/findorders', 'OrderAPIController@find');

        Route::post('v1/acceptedJob', 'JobAPIController@acceptedJob');

        Route::post('v1/cancelOrder', 'OrderAPIController@cancelOrder');

        Route::post('v1/routing', 'OrderAPIController@routing');

        Route::get('v1/scholarships', 'UniversityAPIController@scholarships');

        Route::resource('v1/roles', 'RoleAPIController');

        Route::resource('v1/permissions', 'PermissionAPIController');

        Route::resource('v1/languages', 'LanguageAPIController');

        Route::resource('v1/contactus', 'ContactUsAPIController');

        Route::resource('v1/notifications', 'NotificationAPIController');

        Route::resource('v1/menus', 'MenuAPIController');

        Route::resource('v1/orders', 'OrderAPIController');

        Route::resource('v1/order-details', 'OrderDetailAPIController');

        Route::resource('v1/customers', 'CustomerAPIController');

        Route::resource('v1/washers', 'WasherAPIController');

        Route::post('v1/checkJob', 'CancelledOrderAPIController@checkJob');

        Route::resource('v1/cancelled-orders', 'CancelledOrderAPIController');

        Route::resource('v1/wallets', 'WalletAPIController');

    });

} else {
    ## routes for
}

## No Token Required
Route::resource('v1/universities', 'UniversityAPIController');
Route::post('v1/register', 'AuthAPIController@register')->name('register');

Route::get('v1/verify-email/{code}', 'AuthAPIController@verifyEmail');

Route::post('v1/verify-code', 'AuthAPIController@verifyLoginCode');
Route::post('v1/code', 'AuthAPIController@verifyLoginCode');

Route::post('v1/resend-code', 'AuthAPIController@resendCode')->name('resend-code');

Route::post('v1/login', 'AuthAPIController@login')->name('login');
Route::post('v1/social_login', 'AuthAPIController@socialLogin')->name('socialLogin');
Route::resource('v1/pages', 'PageAPIController');
Route::get('v1/forget-password', 'AuthAPIController@getForgetPasswordCode')->name('forget-password');
Route::post('v1/verify-reset-code', 'AuthAPIController@verifyCode')->name('verify-code');
Route::post('v1/reset-password', 'AuthAPIController@updatePassword')->name('reset-password');
Route::resource('v1/settings', 'SettingAPIController');

Route::resource('v1/reports', 'ReportAPIController');

Route::resource('v1/jobs', 'JobAPIController');

Route::resource('v1/total-scholarships', 'TotalScholarshipAPIController');

Route::resource('v1/refund-customers', 'RefundCustomerAPIController');

