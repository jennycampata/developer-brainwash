@extends('admin.layouts.app')

@section('title')
    {{ $university->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($university, ['route' => ['admin.universities.update', $university->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.universities.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection