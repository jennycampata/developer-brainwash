<!-- Id Field -->
{{--<dt>{!! Form::label('id', 'Id:') !!}</dt>--}}
{{--<dd>{!! $university->id !!}</dd>--}}

<!-- Name Field -->
<dt>{!! Form::label('name', 'Name:') !!}</dt>
<dd>{!! $university->name !!}</dd>

<!-- Status Field -->
<dt>{!! Form::label('status', 'Status:') !!}</dt>

@if($university->status == 1)
<dd>{!! "Active" !!}</dd>
@else
<dd>{!! "Block" !!}</dd>
@endif

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $university->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $university->updated_at !!}</dd>

<!-- Deleted At Field -->
{{--<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>--}}
{{--<dd>{!! $university->deleted_at !!}</dd>--}}

