@extends('admin.layouts.app')

@section('title')
    {{ $cancelledOrder->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($cancelledOrder, ['route' => ['admin.cancelled-orders.update', $cancelledOrder->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.cancelled_orders.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection