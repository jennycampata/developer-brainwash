@extends('admin.layouts.app')

@section('content')
    <div class="content" style="    margin-top: -50px;">
        <h2 style="padding-bottom: 20px; font-size: 4vmin;">Dashboard</h2>
        <div class="row">

            {{--Total Revenue--}}
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>
                    <div class="info-box-content">
                        <span>Total Revenue</span>
                        <span class="info-box-number">${{$total_revenue}}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            {{--Total Scholarships--}}
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-graduation-cap"></i></span>
                    <div class="info-box-content">
                        <span>Total Scholarships</span>
                        <span class="info-box-number">${{ $count }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

            {{--Total Customers--}}
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>
                    <div class="info-box-content">
                        <span>Total Customers</span>
                        <span class="info-box-number">{{ $customers }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>


            {{--Total Washers--}}
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
                    <div class="info-box-content">
                        <span>Total Washers</span>
                        <span class="info-box-number">{{ $washers }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>

        </div>
    </div>
@endsection