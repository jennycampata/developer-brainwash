@extends('admin.layouts.app')

@section('title')
    {{ $washer->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($washer, ['route' => ['admin.washers.update', $washer->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.washers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection