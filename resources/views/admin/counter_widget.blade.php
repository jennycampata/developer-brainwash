<!-- small box -->
<div class="small-box">
    <div class="inner">
        <div class="row">
            <div class="col-md-6 icon" style="background-color: {{ $bgColor }}; text-align: center">
                <i class="{{$icon}}" style="color: white;"></i>
            </div>
            <div class="col-md-2">
                <p>{{ $title }}</p>
                <h3>{{ $counter }}</h3>
            </div>
        </div>
    </div>
</div>