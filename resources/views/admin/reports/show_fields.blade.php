<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $report->id !!}</dd>

<!-- Reported To Field -->
<dt>{!! Form::label('reported_to', 'Reported To:') !!}</dt>
<dd>{!! $report->reported_to !!}</dd>

<!-- Reported By Field -->
<dt>{!! Form::label('reported_from', 'Reported from:') !!}</dt>
<dd>{!! $report->reported_from !!}</dd>

<!-- Reason Field -->
<dt>{!! Form::label('reason', 'Reason:') !!}</dt>
<dd>{!! $report->reason !!}</dd>

<!-- Refund Field -->
<dt>{!! Form::label('refund', 'Refund:') !!}</dt>
<dd>{!! $report->refund !!}</dd>

<!-- Status Field -->
<dt>{!! Form::label('status', 'Status:') !!}</dt>
<dd>{!! $report->status !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $report->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $report->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $report->deleted_at !!}</dd>

