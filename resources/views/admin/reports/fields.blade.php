<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reported_to', 'Reported to:') !!}
    {!! Form::text('reported_to', null, ['class' => 'form-control',  'readonly' => true]) !!}
</div>

<!-- Role Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reported_from', 'reported From:') !!}
    {!! Form::text('reported_from', null, ['class' => 'form-control', 'readonly' => true]) !!}
</div>

<!-- Reason Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reason', 'Reason:') !!}
    {!! Form::text('reason', null, ['class' => 'form-control', 'placeholder'=>'Enter reason']) !!}
</div>

<!-- Refund Field -->
<div class="form-group col-sm-6">
    {!! Form::label('refund', 'Refund:') !!}
    {!! Form::text('refund', null, ['class' => 'form-control', 'placeholder'=>'Enter refund']) !!}
</div>


<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', \App\Models\Report::$dropdown, null,['class' => 'select2 form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($report))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.reports.index') !!}" class="btn btn-default">Cancel</a>
</div>