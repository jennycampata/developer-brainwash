<aside class="main-sidebar" id="sidebar-wrapper">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::user()->details->image_url }}" class="userImg" alt="User Image"/>
            </div>
            <div class="pull-left info" style="padding-top: 13px;">
                <p> {!! Auth::user()->name !!}</p>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            @include('admin.layouts.menu')
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<style>
    .userImg {
        height: 50px;
        width: 50px;
        border-radius: 50%;
    }

    .userName {
        color: white;
        padding-top: 8%;
        text-align: left;
        padding-left: 12px;
    }
</style>