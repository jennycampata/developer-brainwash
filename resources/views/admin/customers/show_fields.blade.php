<!-- Id Field -->
{{--<dt>{!! Form::label('id', 'Id:') !!}</dt>--}}
{{--<dd>{!! $customer->id !!}</dd>--}}

<!-- Name Field -->
<dt>{!! Form::label('name', 'Name:') !!}</dt>
<dd>{!! $customer->name !!}</dd>

<!-- Email Field -->
<dt>{!! Form::label('email', 'Email:') !!}</dt>
<dd>{!! $customer->email !!}</dd>

<!-- Password Field -->
{{--<dt>{!! Form::label('password', 'Password:') !!}</dt>--}}
{{--<dd>{!! $customer->password !!}</dd>--}}

{{--<!-- Remember Token Field -->--}}
{{--<dt>{!! Form::label('remember_token', 'Remember Token:') !!}</dt>--}}
{{--<dd>{!! $customer->remember_token !!}</dd>--}}

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $customer->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $customer->updated_at !!}</dd>

<!-- Deleted At Field -->
{{--<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>--}}
{{--<dd>{!! $customer->deleted_at !!}</dd>--}}

