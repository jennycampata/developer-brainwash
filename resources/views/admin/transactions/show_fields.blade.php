<!-- User Id Field -->
<dt>{!! Form::label('user_id', 'Name:') !!}</dt>
<dd>{!! $transaction->user->name !!}</dd>

<!-- Order Id Field -->
<dt>{!! Form::label('order_id', 'Order Id:') !!}</dt>
<dd>{!! $transaction->order_id !!}</dd>

<!-- Payment Id Field -->
<dt>{!! Form::label('payment_id', 'Payment ID:') !!}</dt>
<dd>{!! $transaction->payment_id !!}</dd>

<!-- Payment Method Field -->
<dt>{!! Form::label('payment_method', 'Payment Method:') !!}</dt>
<dd>{!! $transaction->payment_method !!}</dd>

<!-- Amount Field -->
<dt>{!! Form::label('amount', 'Amount:') !!}</dt>
<dd>{!! $transaction->amount !!}</dd>

<!-- State Field -->
<dt>{!! Form::label('state', 'State:') !!}</dt>
<dd>{!! $transaction->state !!}</dd>

<!-- Details Field -->
<dt>{!! Form::label('details', 'Details:') !!}</dt>
<dd>{!! $transaction->details !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $transaction->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $transaction->updated_at !!}</dd>

{{--<!-- Deleted At Field -->--}}
{{--<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>--}}
{{--<dd>{!! $transaction->deleted_at !!}</dd>--}}

