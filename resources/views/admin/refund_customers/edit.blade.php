@extends('admin.layouts.app')

@section('title')
    {{ $refundCustomer->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($refundCustomer, ['route' => ['admin.refund-customers.update', $refundCustomer->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.refund_customers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection