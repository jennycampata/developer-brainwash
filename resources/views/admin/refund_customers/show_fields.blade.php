<!-- Id Field -->
{{--<dt>{!! Form::label('id', 'Id:') !!}</dt>--}}
{{--<dd>{!! $refundCustomer->id !!}</dd>--}}

<!-- User Id Field -->
<dt>{!! Form::label('user_id', 'Customer Name:') !!}</dt>
<dd>{!! $refundCustomer->user->name !!}</dd>

<!-- University Id Field -->
<dt>{!! Form::label('university_id', 'University:') !!}</dt>
<dd>{!! $refundCustomer->university->name !!}</dd>

<!-- Amount Field -->
<dt>{!! Form::label('amount', 'Amount:') !!}</dt>
<dd>{!! '$' . $refundCustomer->amount !!}</dd>

<!-- Status Field -->
<dt>{!! Form::label('status', 'Status:') !!}</dt>

@if($refundCustomer->status == 0)
<dd>{!! 'Refunded' !!}</dd>
@else
    <dd>{!! 'Pending' !!}</dd>
    @endif

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $refundCustomer->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $refundCustomer->updated_at !!}</dd>

<!-- Deleted At Field -->
{{--<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>--}}
{{--<dd>{!! $refundCustomer->deleted_at !!}</dd>--}}

