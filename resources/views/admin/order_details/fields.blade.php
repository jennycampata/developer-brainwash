<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control', 'placeholder'=>'Enter order_id']) !!}
</div>

<!-- No Bags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_bags', 'No Bags:') !!}
    {!! Form::text('no_bags', null, ['class' => 'form-control', 'placeholder'=>'Enter no_bags']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control', 'placeholder'=>'Enter type']) !!}
</div>

<!-- Detergent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('detergent', 'Detergent:') !!}
    {!! Form::text('detergent', null, ['class' => 'form-control', 'placeholder'=>'Enter detergent']) !!}
</div>

<!-- Folded Field -->
<div class="form-group col-sm-6">
    {!! Form::label('folded', 'Folded:') !!}
    {!! Form::text('folded', null, ['class' => 'form-control', 'placeholder'=>'Enter folded']) !!}
</div>

<!-- Hung Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hung', 'Hung:') !!}
    {!! Form::text('hung', null, ['class' => 'form-control', 'placeholder'=>'Enter hung']) !!}
</div>

<!-- Instruction Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instruction', 'Instruction:') !!}
    {!! Form::text('instruction', null, ['class' => 'form-control', 'placeholder'=>'Enter instruction']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder'=>'Enter price']) !!}
</div>

<!-- Donation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('donation', 'Donation:') !!}
    {!! Form::text('donation', null, ['class' => 'form-control', 'placeholder'=>'Enter donation']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($orderDetail))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.order-details.index') !!}" class="btn btn-default">Cancel</a>
</div>