@extends('admin.layouts.app')

@section('title')
    {{ $orderDetail->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($orderDetail, ['route' => ['admin.order-details.update', $orderDetail->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.order_details.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection