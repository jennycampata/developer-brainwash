<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $orderDetail->id !!}</dd>

<!-- Order Id Field -->
<dt>{!! Form::label('order_id', 'Order Id:') !!}</dt>
<dd>{!! $orderDetail->order_id !!}</dd>

<!-- No Bags Field -->
<dt>{!! Form::label('no_bags', 'No Bags:') !!}</dt>
<dd>{!! $orderDetail->no_bags !!}</dd>

<!-- Type Field -->
<dt>{!! Form::label('type', 'Type:') !!}</dt>
<dd>{!! $orderDetail->type !!}</dd>

<!-- Detergent Field -->
<dt>{!! Form::label('detergent', 'Detergent:') !!}</dt>
<dd>{!! $orderDetail->detergent !!}</dd>

<!-- Folded Field -->
<dt>{!! Form::label('folded', 'Folded:') !!}</dt>
<dd>{!! $orderDetail->folded !!}</dd>

<!-- Hung Field -->
<dt>{!! Form::label('hung', 'Hung:') !!}</dt>
<dd>{!! $orderDetail->hung !!}</dd>

<!-- Instruction Field -->
<dt>{!! Form::label('instruction', 'Instruction:') !!}</dt>
<dd>{!! $orderDetail->instruction !!}</dd>

<!-- Price Field -->
<dt>{!! Form::label('price', 'Price:') !!}</dt>
<dd>{!! $orderDetail->price !!}</dd>

<!-- Donation Field -->
<dt>{!! Form::label('donation', 'Donation:') !!}</dt>
<dd>{!! $orderDetail->donation !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $orderDetail->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $orderDetail->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $orderDetail->deleted_at !!}</dd>

