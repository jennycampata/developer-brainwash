@extends('admin.layouts.app')

@section('title')
    {{ $totalScholarship->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($totalScholarship, ['route' => ['admin.total-scholarships.update', $totalScholarship->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.total_scholarships.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection