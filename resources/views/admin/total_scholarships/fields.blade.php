<!-- University Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('university_id', 'University Id:') !!}
    {!! Form::text('university_id', null, ['class' => 'form-control', 'placeholder'=>'Enter university_id']) !!}
</div>

<!-- Total Raised Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_raised', 'Total Raised:') !!}
    {!! Form::text('total_raised' , null, ['class' => 'form-control', 'placeholder'=>'Enter total_raised']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($totalScholarship))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.total-scholarships.index') !!}" class="btn btn-default">Cancel</a>
</div>