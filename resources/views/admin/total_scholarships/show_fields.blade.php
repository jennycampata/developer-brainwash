<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $totalScholarship->id !!}</dd>

<!-- University Id Field -->
<dt>{!! Form::label('university_id', 'University Id:') !!}</dt>
<dd>{!! $totalScholarship->university->name !!}</dd>

<!-- Total Raised Field -->
<dt>{!! Form::label('total_raised', 'Total Raised:') !!}</dt>
<dd>{!! $totalScholarship->total_raised !!}</dd>

{{--<!-- Created At Field -->--}}
{{--<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>--}}
{{--<dd>{!! $totalScholarship->created_at !!}</dd>--}}

{{--<!-- Updated At Field -->--}}
{{--<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>--}}
{{--<dd>{!! $totalScholarship->updated_at !!}</dd>--}}

{{--<!-- Deleted At Field -->--}}
{{--<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>--}}
{{--<dd>{!! $totalScholarship->deleted_at !!}</dd>--}}

