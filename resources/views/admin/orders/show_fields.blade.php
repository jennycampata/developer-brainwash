<style>
    #myMap {
        width: 700px;
        height: 350px;
    }

    #yourMap {
        width: 700px;
        height: 350px;
    }
</style>

<div class="col-md-4">
    {{--<!-- Id Field -->--}}
    {{--<dt>{!! Form::label('id', 'Id:') !!}</dt>--}}
    {{--<dd>{!! $order->id !!}</dd>--}}

    <!-- User Id Field -->
    <dt>{!! Form::label('user_id', 'Name:') !!}</dt>
    <dd>{!! $order->user->name !!}</dd>

{{--<!-- Up Latitude Field -->--}}
{!! Form::hidden('up_latitude', $order->up_latitude, ['class' => 'form-control up_lat']) !!}

{{--<!-- Up Longitude Field -->--}}
{!! Form::hidden('up_longitude', $order->up_longitude, ['class' => 'form-control up_lng']) !!}

{{--<!-- Down Latitude Field -->--}}
{!! Form::hidden('down_latitude', $order->down_latitude, ['class' => 'form-control down_lat']) !!}

{{--<!-- Down Longitude Field -->--}}
{!! Form::hidden('down_longitude', $order->down_longitude, ['class' => 'form-control down_lng']) !!}

<!-- No Bags Field -->
    <dt>{!! Form::label('no_bags', 'Number of Bags:') !!}</dt>
    <dd>{!! $order->details->no_bags !!}</dd>

<!-- Type Field -->
    <dt>{!! Form::label('type', 'Type:') !!}</dt>
    @if($order->details->type == 0)
    <dd>{!! "No thanks" !!}</dd>
    @else
        <dd>{!! "Same gender" !!}</dd>
    @endif

<!-- Detergent Field -->
    <dt>{!! Form::label('detergent', 'Detergent:') !!}</dt>
    @if($order->details->detergent == 0)
        <dd>{!! "No" !!}</dd>
    @else
        <dd>{!! "Own" !!}</dd>
@endif


<!-- Folded Field -->
    <dt>{!! Form::label('folded', 'Folded:') !!}</dt>
    @if($order->details->folded == 0)
        <dd>{!! "No" !!}</dd>
    @else
        <dd>{!! "Yes" !!}</dd>
@endif


<!-- Hung Field -->
    <dt>{!! Form::label('hung', 'Hung:') !!}</dt>
    @if($order->details->hung == 0)
        <dd>{!! "No" !!}</dd>
    @else
        <dd>{!! "Yes" !!}</dd>
@endif


<!-- Instruction Field -->
    <dt>{!! Form::label('instruction', 'Instruction:') !!}</dt>
<dd>{!! $order->details->instruction !!}</dd>

<!-- Price Field -->
    <dt>{!! Form::label('price', 'Price:') !!}</dt>
<dd>{!! $order->details->price !!}</dd>

<!-- Donation Field -->
    <dt>{!! Form::label('donation', 'Donation:') !!}</dt>
<dd>{!! "$" . $order->details->donation !!}</dd>

<!-- Status Field -->
    <dt>{!! Form::label('status', 'Status:') !!}</dt>
    @if($order->status == 0)
        <dd>{!! "None" !!}</dd>
    @elseif($order->status == 1)
        <dd>{!! "In Progress" !!}</dd>
        @else
        <dd>{!! "Complete" !!}</dd>
    @endif

</div>


<!-- Tab -->
<div class="col-md-8">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#pickup" data-toggle="tab">Pickup Location</a></li>
            <li><a href="#dropoff" data-toggle="tab">Dropoff Location</a></li>
        </ul>

        {{--Pickup Location--}}
        <div class="tab-content">
            <div class="tab-pane active" id="pickup">

                {{--Pick-up Location--}}
                @if(isset($order->pick_up))
                    <dt>{!! Form::label('pick_up', 'Pick-up Location:') !!}</dt>
                    <dd>{!! $order->pick_up !!}</dd>
                @endif
                <dl class="dl-horizontal">
                    <div class="container">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div id="myMap"></div>
                            </div>

                        </div>
                    </div>

                </dl>
            </div>

            {{--Dropoff Location--}}
            <div class="tab-pane" id="dropoff">

                {{--Drop-off Location--}}
                @if(isset($order->drop_off))
                    <dt>{!! Form::label('drop_off', 'Drop-off Location:') !!}</dt>
                    <dd>{!! $order->drop_off !!}</dd>
                @endif
                <dl class="dl-horizontal">
                    <div class="container">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div id="yourMap"></div>
                            </div>

                        </div>
                    </div>

                </dl>
            </div>
        </div>
    </div>
</div>

@push('scripts')

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="{{ url('public/js/admin/locationpicker.jquery.min.js') }}"></script>

    <script src=" http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyAgYs27nzsBRyhbwRtziWCJT7hYxdVyryo"
            type="text/javascript"></script>


    <script>
        var up_lat = $('.up_lat').val();
        var up_lng = $('.up_lng').val();

        var down_lat = $('.down_lat').val();
        var down_lng = $('.down_lng').val();


        var map;
        var marker;
        var myLatlng = new google.maps.LatLng(up_lat, up_lng);
        var yourLatlng = new google.maps.LatLng(down_lat, down_lng);

        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();

        function initialize() {
            var mapOptions = {
                zoom: 18,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var yourmapOptions = {
                zoom: 18,
                center: yourLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("myMap"), mapOptions);
            yourmap = new google.maps.Map(document.getElementById("yourMap"), yourmapOptions);

            marker = new google.maps.Marker({
                map: map,
                position: myLatlng,
                draggable: false
            });

            yourmarker = new google.maps.Marker({
                map: yourmap,
                position: yourLatlng,
                draggable: false
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

@endpush
