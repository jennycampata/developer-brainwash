<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::text('user_id', null, ['class' => 'form-control', 'placeholder'=>'Enter user_id', 'readonly']) !!}
</div>

<!-- Up Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('up_latitude', 'Up Latitude:') !!}
    {!! Form::text('up_latitude', null, ['class' => 'form-control', 'placeholder'=>'Enter up_latitude']) !!}
</div>

<!-- Up Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('up_longitude', 'Up Longitude:') !!}
    {!! Form::text('up_longitude', null, ['class' => 'form-control', 'placeholder'=>'Enter up_longitude']) !!}
</div>

<!-- Down Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('down_latitude', 'Down Latitude:') !!}
    {!! Form::text('down_latitude', null, ['class' => 'form-control', 'placeholder'=>'Enter down_latitude']) !!}
</div>

<!-- Down Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('down_longitude', 'Down Longitude:') !!}
    {!! Form::text('down_longitude', null, ['class' => 'form-control', 'placeholder'=>'Enter down_longitude']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', \App\Models\Order::$status, null, ['class' => 'select2 form-control', 'placeholder'=>'Enter status']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($order))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.orders.index') !!}" class="btn btn-default">Cancel</a>
</div>