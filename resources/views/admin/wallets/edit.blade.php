@extends('admin.layouts.app')

@section('title')
    {{ $wallet->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($wallet, ['route' => ['admin.wallets.update', $wallet->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.wallets.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection