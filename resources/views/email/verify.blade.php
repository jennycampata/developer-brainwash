<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

<h2>Welcome to the site, {{ $name }}. </h2>
<br/>
Please click on the below link to verify your email account.
<br/>
<a href="{{url('/api/v1/verify-email/'. $email_code)}}">Verify Email</a>
<br/>
If you didn't request, please ignore this email.
</body>
</html>