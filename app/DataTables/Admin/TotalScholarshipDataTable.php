<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\TotalScholarship;
use App\Models\University;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class TotalScholarshipDataTable
 * @package App\DataTables\Admin
 */
class TotalScholarshipDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('university_id', function ($university) {
            return $university->university->name;
        });

        $dataTable->rawColumns(['university', 'total_raised', 'action']);

        return $dataTable->addColumn('action', 'admin.total_scholarships.datatables_actions');
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\TotalScholarship $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(TotalScholarship $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('total-scholarships.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'university_id' => ['title' => 'University'],
            'total_raised'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'total_scholarshipsdatatable_' . time();
    }
}