<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class CustomerDataTable
 * @package App\DataTables\Admin
 */
class CustomerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('university', function ($university) {
            if (isset($university->user_university->university->name)) {
                return $university->user_university->university->name;
            } else {
                return 'Not listed';
            }
        });

        $dataTable->addColumn('price', function ($model) {
            if (isset($model->orders)) {
                $count = 0;
                foreach ($model->orders as $item) {
                    $detail = OrderDetail::where('order_id', $item->id)->get();
                    foreach ($detail as $row) {
                        $count += $row->price;
                    }
                }
                return '$' . round($count);
            } else {
                return 0;
            }
        });

        $dataTable->addColumn('status', function ($status) {
            if (isset($status->details->is_verified)) {
                if ($status->details->is_verified) {
                    return '<button type="button" class="btn btn-primary changeStatus" data-id="' . $status->id . '">Active</button>';
                } else {
                    return '<button type="button" class="btn btn-warning changeStatus"  data-id="' . $status->id . '">Block</button>';
                }
            }

        });


        $dataTable->addColumn('total_washed', function ($model) {
            if (isset($model)) {
                return $model->orders->count();
            } else {
                return 'No orders';
            }
        });
        $dataTable->rawColumns(['university', 'total_washed', 'price', 'status', 'action']);
        return $dataTable->addColumn('action', 'admin.customers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Customer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Customer $model)
    {
        return $model->newQuery()->orderBy('updated_at', SORT_DESC)->whereHas
        ('roles', function ($a) {
            return $a->where('id', Customer::CUSTOMER);
        });
        //return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
//        if (\Entrust::can('customers.create') || \Entrust::hasRole('super-admin')) {
//            $buttons = ['create'];
//        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);

        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
            'email',
            'university',
            'total_washed',
            'price',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'customersdatatable_' . time();
    }
}