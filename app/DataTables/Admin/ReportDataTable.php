<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\Report;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class ReportDataTable
 * @package App\DataTables\Admin
 */
class ReportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('reported_to', function ($reportTo) {
            return $reportTo->reportedTo->name;
        });

        $dataTable->addColumn('reported_from', function ($reportFrom) {
            return $reportFrom->reportedBy->name;
        });

        $dataTable->addColumn('status', function ($status) {
            if ($status->status == 0) {
                return '<button type="button" class="btn btn-primary" data-id="' . $status->id . '">Resolved</button>';
            } elseif ($status->status == 1) {
                return '<button type="button" class="btn btn-warning"  data-id="' . $status->id . '">Unresolved</button>';
            } elseif ($status->status == 2) {
                return '<button type="button" class="btn btn-danger"  data-id="' . $status->id . '">Blocked</button>';
            }
        });

        $dataTable->rawColumns(['reported_from', 'reported_to', 'status', 'action']);

        return $dataTable->addColumn('action', 'admin.reports.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Report $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Report $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('reports.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
       }

        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'reported_from',
            'reported_to',
            'reason',
//            'refund',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'reportsdatatable_' . time();
    }
}