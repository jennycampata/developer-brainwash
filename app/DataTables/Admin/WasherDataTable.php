<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Washer;
use function foo\func;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class WasherDataTable
 * @package App\DataTables\Admin
 */
class WasherDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('status', function ($status) {
            if (isset($status->details->is_verified)) {
                if ($status->details->is_verified) {
                    return '<button type="button" class="btn btn-primary changeStatus" data-id="' . $status->id . '">Active</button>';
                } else {
                    return '<button type="button" class="btn btn-warning changeStatus"  data-id="' . $status->id . '">Block</button>';
                }
            }
        });

        $dataTable->addColumn('price', function ($model) {
            if (isset($model->orders)) {
                $count = 0;
                foreach ($model->orders as $item) {
                    $detail = OrderDetail::where('id', $item->id)->get();
                    foreach ($detail as $row) {
                        $count += $row->price;
                    }
                }
                return '$' . round($count);
            } else {
                return 0;
            }
        });

    $dataTable->addColumn('in_washing', function($model){
       if (isset($model->orders)){
           $completed = 0;
           foreach ($model->orders as $item){
               $completed = Order::where('user_id', $item->user_id)->where('status', '!=', Order::COMPLETED)->count();
           }
           return $completed;
       }
    });

    $dataTable->addColumn('washes_completed', function ($model){
       if (isset($model->orders)){
           $completed = 0;
           foreach ($model->orders as $item){
               $completed = Order::where('user_id', $item->user_id)->where('status', '=', Order::COMPLETED)->count();
           }
           return $completed;
       }
    });

        $dataTable->rawColumns(['in_washing', 'washes_completed', 'price', 'status', 'action']);
        return $dataTable->addColumn('action', 'admin.washers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Washer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Washer $model)
    {
        return $model->newQuery()->orderBy('updated_at', SORT_DESC)->whereHas
        ('roles', function ($a) {
            return $a->where('id', Washer::WASHER);
        });
        // return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
//        if (\Entrust::can('washers.create') || \Entrust::hasRole('super-admin')) {
//            $buttons = ['create'];
//        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);

        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
            'email',
            'in_washing',
            'washes_completed',
            'price',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'washersdatatable_' . time();
    }
}