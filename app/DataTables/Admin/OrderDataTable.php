<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\Order;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class OrderDataTable
 * @package App\DataTables\Admin
 */
class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('status', function ($status) {
            if ($status->status == 0) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">NEW</button>';
            } elseif ($status->status == 1) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">ACCEPTED</button>';
            } elseif ($status->status == 2) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">PICK-UP</button>';
            } elseif ($status->status == 3) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">WASH</button>';
            } elseif ($status->status == 4) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">DRIER</button>';
            } elseif ($status->status == 5) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">FOLDING</button>';
            } elseif ($status->status == 6) {
                return '<button type="button" style="width:107px;" class="btn btn-primary" data-id="' . $status->id . '">DROP-OFF</button>';
            } elseif ($status->status == 7) {
                return '<button type="button" class="btn btn-primary" data-id="' . $status->id . '">COMPLETED</button>';
            }
        });

        $dataTable->addColumn('user_id', function ($order) {
            return $order->user->name;
        });

        $dataTable->rawColumns(['user_id', 'status', 'action']);

        return $dataTable->addColumn('action', 'admin.orders.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('orders.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user_id' => ['title' => 'Name'],
            'pick_up',
            'drop_off',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ordersdatatable_' . time();
    }
}