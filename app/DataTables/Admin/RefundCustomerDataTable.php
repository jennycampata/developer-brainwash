<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\RefundCustomer;
use App\Models\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class RefundCustomerDataTable
 * @package App\DataTables\Admin
 */
class RefundCustomerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('email', function($model){
           return $model->user->email;
        });

        $dataTable->addColumn('user_id', function ($model){
            return $model->user->name;
        });

        $dataTable->addColumn('university_id', function ($model){
            return $model->university->name;
        });

        $dataTable->addColumn('amount', function ($model){
            return '$' . $model->amount;
        });

        $dataTable->addColumn('status', function ($model){

            if ($model->status) {
                return '<button type="button" class="btn btn-primary refundBtn" data-id="' . $model->id . '">Pending</button>';
            } else {
                return '<button type="button" class="btn btn-warning changeRefundStatus"  data-id="' . $model->id . '">Refunded</button>';
            }
        });

        $dataTable->rawColumns(['user_id', 'email', 'university', 'amount', 'status', 'action']);

        return $dataTable->addColumn('action', 'admin.refund_customers.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\RefundCustomer $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RefundCustomer $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('refund-customers.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user_id' => ['title' => 'Name'],
            'email',
            'university_id' => ['title' => 'University'],
            'amount',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'refund_customersdatatable_' . time();
    }
}