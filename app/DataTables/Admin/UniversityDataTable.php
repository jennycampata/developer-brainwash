<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\TotalScholarship;
use App\Models\University;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class UniversityDataTable
 * @package App\DataTables\Admin
 */
class UniversityDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('status', function ($status) {

            if ($status->status) {
                return '<button type="button" class="btn btn-primary changeUniStatus" data-id="' . $status->id . '">Active</button>';
            } else {
                return '<button type="button" class="btn btn-warning changeUniStatus"  data-id="' . $status->id . '">Block</button>';
            }
        });

        $dataTable->addColumn('total_raised', function ($model) {
            if (isset($model->scholarships)) {
                $count = 0;
                foreach ($model->scholarships as $row) {
                    $count += $row->total_raised;
                }
                return '$' . $count;
            }
        });
        // show that record in total scholarship data table with possibly, join

        $dataTable->rawColumns(['status', 'total_raised', 'action']);

        return $dataTable->addColumn('action', 'admin.universities.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\University $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(University $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('universities.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name',
            'total_raised',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'universitiesdatatable_' . time();
    }
}