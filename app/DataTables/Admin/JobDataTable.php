<?php

namespace App\DataTables\Admin;

use App\Helper\Util;
use App\Models\Job;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class JobDataTable
 * @package App\DataTables\Admin
 */
class JobDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->addColumn('user_id', function ($name) {
            return $name->user->name;
        });

        $dataTable->addColumn('order_id', function ($order) {
            return $order->order_id;
        });

        $dataTable->addColumn('status', function ($status) {
            if ($status->status) {
                return '<button type="button" class="btn btn-primary changeJobStatus" data-id="' . $status->id . '">Active</button>';
            } else {
                return '<button type="button" class="btn btn-warning changeJobStatus"  data-id="' . $status->id . '">Block</button>';
            }
        });

        $dataTable->rawColumns(['user_id', 'order_id', 'status', 'action']);

        return $dataTable->addColumn('action', 'admin.jobs.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Job $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Job $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('jobs.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters(array_merge(Util::getDataTableParams(), [
                'dom'     => 'Blfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'order_id'  => ['title' => 'Order'],
            'user_id'   => ['title' => 'Name'],
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'jobsdatatable_' . time();
    }
}