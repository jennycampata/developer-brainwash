<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateJobAPIRequest;
use App\Http\Requests\Api\UpdateJobAPIRequest;
use App\Models\Job;
use App\Models\Notification;
use App\Models\Order;
use App\Repositories\Admin\JobRepository;
use App\Repositories\Admin\NotificationRepository;
use App\Repositories\Admin\OrderRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class JobController
 * @package App\Http\Controllers\Api
 */
class JobAPIController extends AppBaseController
{
    /** @var  JobRepository */
    private $jobRepository;

    /** @var  OrderRepository */
    private $orderRepository;

    /** @var  UserRepository */
    private $userRepository;

    /** @var  NotificationRepository */
    private $notificationRepository;


    public function __construct(JobRepository $jobRepo, OrderRepository $orderRepo, UserRepository $userRepo, NotificationRepository $notificationRepo)
    {
        $this->jobRepository = $jobRepo;
        $this->orderRepository = $orderRepo;
        $this->userRepository = $userRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/jobs",
     *      summary="Get a listing of the Jobs.",
     *      tags={"Job"},
     *      description="Get all Jobs",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Job")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->jobRepository->pushCriteria(new RequestCriteria($request));
        $this->jobRepository->pushCriteria(new LimitOffsetCriteria($request));
//        $jobs = $this->jobRepository->findWhere(['user_id'=> $request->]);
        $jobs = $this->jobRepository->all();

        return $this->sendResponse($jobs->toArray(), 'Jobs retrieved successfully');
    }

    /**
     * @param CreateJobAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/jobs",
     *      summary="Store a newly created Job in storage",
     *      tags={"Job"},
     *      description="Store Job",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Job that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Job")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Job"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateJobAPIRequest $request)
    {
        $order = $this->orderRepository->find($request->order_id);
        $ord = $this->jobRepository->findWhere(['user_id' => $request->user_id])->where('status', 0)->count();

        //Washer Can't Accept Job Twice..
        if ($ord >= 2) {
            $status = false;
            return $this->sendErrorWithData(['Please complete your pending orders before accepting a new one.'], 401);
        }

        $jobs = $this->jobRepository->saveRecord($request);
        $washer = $this->userRepository->find($request->user_id);
        $input = [];
        $input['status'] = 1;
        Order::where('id', $order->id)->update($input);
        $message = array_values(Notification::ACCEPT)[0] . $washer->name;
        $notification = [
            'sender_id'   => $request->user_id,
            'action_type' => array_keys(Notification::ACCEPT)[0],
            'url'         => null,
            //'title'       => array_keys(Notification::TITLE)[0],
            'ref_id'      => $order->id,
            'message'     => $message
        ];

        $this->notificationRepository->notification($notification, $order->user_id);
        return $this->sendResponse($jobs->toArray(), 'Job saved successfully');

        //check if order is completed
        //if it is then change status of proposals to 1
        //take out the washer ID
        //check the orders related to that washer ID

    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/jobs/{id}",
     *      summary="Display the specified Job",
     *      tags={"Job"},
     *      description="Get Job",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Job",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Job"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Job $job */
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            return $this->sendError('Job not found');
        }

        return $this->sendResponse($job->toArray(), 'Job retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateJobAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/jobs/{id}",
     *      summary="Update the specified Job in storage",
     *      tags={"Job"},
     *      description="Update Job",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Job",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Job that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Job")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Job"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateJobAPIRequest $request)
    {
        /** @var Job $job */
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            return $this->sendError('Job not found');
        }

        $job = $this->jobRepository->updateRecord($request, $id);

        return $this->sendResponse($job->toArray(), 'Job updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/jobs/{id}",
     *      summary="Remove the specified Job from storage",
     *      tags={"Job"},
     *      description="Delete Job",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Job",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Job $job */
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            return $this->sendError('Job not found');
        }

        $this->jobRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Job deleted successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\POST(
     *      path="/acceptedJob",
     *      summary="Get accepted Job",
     *      tags={"Job"},
     *      description="Show the accepted Job",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="Washer id",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Show the accepted Job",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AcceptedJob")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AcceptedJob"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function acceptedJob(Request $request)
    {
        $jobs = $this->jobRepository->findWhere(['user_id' => $request->user_id])->pluck('order_id');
        $order = Order::whereIn('id', $jobs)->get();
        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }
}