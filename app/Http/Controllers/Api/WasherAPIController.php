<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateWasherAPIRequest;
use App\Http\Requests\Api\UpdateWasherAPIRequest;
use App\Models\Washer;
use App\Repositories\Admin\WasherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class WasherController
 * @package App\Http\Controllers\Api
 */

class WasherAPIController extends AppBaseController
{
    /** @var  WasherRepository */
    private $washerRepository;

    public function __construct(WasherRepository $washerRepo)
    {
        $this->washerRepository = $washerRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/washers",
     *      summary="Get a listing of the Washers.",
     *      tags={"Washer"},
     *      description="Get all Washers",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Washer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->washerRepository->pushCriteria(new RequestCriteria($request));
        $this->washerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $washers = $this->washerRepository->all();

        return $this->sendResponse($washers->toArray(), 'Washers retrieved successfully');
    }

    /**
     * @param CreateWasherAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/washers",
     *      summary="Store a newly created Washer in storage",
     *      tags={"Washer"},
     *      description="Store Washer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Washer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Washer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Washer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWasherAPIRequest $request)
    {
        $washers = $this->washerRepository->saveRecord($request);

        return $this->sendResponse($washers->toArray(), 'Washer saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/washers/{id}",
     *      summary="Display the specified Washer",
     *      tags={"Washer"},
     *      description="Get Washer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Washer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Washer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Washer $washer */
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            return $this->sendError('Washer not found');
        }

        return $this->sendResponse($washer->toArray(), 'Washer retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWasherAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/washers/{id}",
     *      summary="Update the specified Washer in storage",
     *      tags={"Washer"},
     *      description="Update Washer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Washer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Washer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Washer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Washer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWasherAPIRequest $request)
    {
        /** @var Washer $washer */
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            return $this->sendError('Washer not found');
        }

        $washer = $this->washerRepository->updateRecord($request, $id);

        return $this->sendResponse($washer->toArray(), 'Washer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/washers/{id}",
     *      summary="Remove the specified Washer from storage",
     *      tags={"Washer"},
     *      description="Delete Washer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Washer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Washer $washer */
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            return $this->sendError('Washer not found');
        }

        $this->washerRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Washer deleted successfully');
    }
}
