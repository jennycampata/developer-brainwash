<?php

namespace App\Http\Controllers\Api;

use App\Criteria\OrderCriteria;
use App\Criteria\UserCriteria;
use App\Http\Requests\Api\CreateOrderAPIRequest;
use App\Http\Requests\Api\UpdateOrderAPIRequest;
use App\Models\Job;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\TotalScholarship;
use App\Models\University;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\UserUniversity;
use App\Models\Wallet;
use App\Models\Washer;
use App\Repositories\Admin\CancelledOrderRepository;
use App\Repositories\Admin\JobRepository;
use App\Repositories\Admin\NotificationRepository;
use App\Repositories\Admin\OrderDetailRepository;
use App\Repositories\Admin\OrderRepository;
use App\Repositories\Admin\UserRepository;
use App\Repositories\Admin\WalletRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Laracasts\Flash\Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class OrderController
 * @package App\Http\Controllers\Api
 */
class OrderAPIController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;
    private $orderDetailRepository;
    private $userRepository;
    private $jobRepository;
    private $notificationRepository;
    private $walletRepository;

    public function __construct(JobRepository $jobRepo, OrderRepository $orderRepo, OrderDetailRepository $orderDetailRepository, UserRepository $userRepository, NotificationRepository $notificationRepo, WalletRepository $walletRepository)
    {
        $this->orderRepository = $orderRepo;
        $this->orderDetailRepository = $orderDetailRepository;
        $this->userRepository = $userRepository;
        $this->jobRepository = $jobRepo;
        $this->notificationRepository = $notificationRepo;
        $this->walletRepository = $walletRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/orders",
     *      summary="Get a listing of the Orders.",
     *      tags={"Order"},
     *      description="Get all Orders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Order")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $user = Auth::id();
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $orders = $this->orderRepository->orderBy('id', 'DESC')->findWhere(['user_id' => $user]);


        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * @param CreateOrderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orders",
     *      summary="Store a newly created Order in storage",
     *      tags={"Order"},
     *      description="Store Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOrderAPIRequest $request)
    {
        $orders = $this->orderRepository->saveRecord($request);

        $request['order_id'] = $orders->id;
        $orders = $this->orderDetailRepository->saveRecord($request);
        $uniId = UserUniversity::where(['user_id' => $request->user_id])->first();
        $message = array_values(Notification::NEW)[0];
        $notification = [
            'sender_id'   => Auth::id(),
            'action_type' => array_keys(Notification::NEW)[0],
            'url'         => null,
            'ref_id'      => $orders->id,
            'message'     => $message
        ];

        //take the requested user id
        $userId = $request->user_id;

        //fetch the role associated with it
        $user = $this->userRepository
            ->resetCriteria()
            ->pushCriteria(new UserCriteria(['role' => User::WASHER]))
            ->get();

        //find the closest washer to that customer

        //send notifications to all washers
        foreach ($user as $row) {
            $this->notificationRepository->notification($notification, $row->id);
        }

        //store the value inside wallet
//        $output = [];
//        $output['amount'] = $request->price;
//        $output['order_id'] = $orders->id;
//        $output['user_id'] = $request->user_id;
//        $output['status'] = 0;
//        Wallet::create($output);

        return $this->sendResponse($orders->toArray(), 'Order saved successfully');
    }


    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orders/{id}",
     *      summary="Display the specified Order",
     *      tags={"Order"},
     *      description="Get Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrderAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orders/{id}",
     *      summary="Update the specified Order in storage",
     *      tags={"Order"},
     *      description="Update Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update($id, Request $request)
    {
        /** @var Order $order */
        $order = $this->orderRepository->findWithoutFail($id);
        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $message = '';
        if ($request->status == 0) {
            $action_type = array_keys(Notification::NEW)[0];
        } elseif ($request->status == 1) {
            $action_type = array_keys(Notification::ACCEPT)[0];
        } elseif ($request->status == 2) {
            $message = array_values(Notification::ON_WAY)[0];
            $action_type = array_keys(Notification::ON_WAY)[0];
        } elseif ($request->status == 3) {
            $message = array_values(Notification::WASH)[0];
            $action_type = array_keys(Notification::WASH)[0];
        } elseif ($request->status == 4) {
            $message = array_values(Notification::DRY)[0];
            $action_type = array_keys(Notification::DRY)[0];
        } elseif ($request->status == 5) {
            $message = array_values(Notification::FOLD)[0];
            $action_type = array_keys(Notification::FOLD)[0];
        } elseif ($request->status == 6) {
            $message = array_values(Notification::DROP)[0];
            $action_type = array_keys(Notification::DROP)[0];
        } else {
            $message = array_values(Notification::COMPLETE)[0];
            $action_type = array_keys(Notification::COMPLETE)[0];
            $input = [];
            $input['status'] = 1;
            Job::where('order_id', $order->id)->update($input);

        }

        $notification = [
            'sender_id'   => Auth::id(),
            'action_type' => $action_type,
            'url'         => null,
            'title'       => array_keys(Notification::TITLE)[0],
            'ref_id'      => $order->id,
            'message'     => $message
        ];
        $order = $this->orderRepository->updateRecord($request, $order);
        $this->notificationRepository->notification($notification, $order->user_id);

        return $this->sendResponse($order->toArray(), 'Order updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orders/{id}",
     *      summary="Remove the specified Order from storage",
     *      tags={"Order"},
     *      description="Delete Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $this->orderRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Order deleted successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/findorders",
     *      summary="Get a nearest Wash",
     *      tags={"Order"},
     *      description="Store Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FindWash")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FindWash"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function find(Request $request)
    {
        $user = Auth::id();
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));

        $lat = $request->latitude;
        $lng = $request->longitude;

        $input = [];
        $input['latitude'] = $lat;
        $input['longitude'] = $lng;

        UserDetail::where('user_id', $user)->update($input);

//        if (isset($lat) && isset($lng)) {
//            $this->orderRepository->pushCriteria(new OrderCriteria([
//                'up_longitude' => $request->longitude,
//                'up_latitude'  => $request->latitude,
//            ]));
//        }

//        $orders = $this->orderRepository->findWhere(['status']);
        $orders = $this->orderRepository->orderBy('id', 'DESC')->findWhere(['status' => Order::NEW]);

//        $orders = $this->orderRepository->all();

        return $this->sendResponse($orders, 'Orders retrieved successfully');

    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/routing",
     *      summary="Check completed Orders",
     *      tags={"Order"},
     *      description="Completed Orders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/routing")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/routing"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function routing(Request $request)
    {
        $user = $this->userRepository->find($request->user_id);
        if ($user->role_id == User::WASHER) {
            $jobs = $this->jobRepository->findWhere(['user_id' => $request->user_id])->where('status', 0);
            $count = 0;
            $order = 0;

            if ($jobs->count() > 0) {
                foreach ($jobs as $raw) {
                    $order = $this->orderRepository->find($raw->order_id)->where('status', '!=', Order::COMPLETED);
//            !!! PENDING !!!
                    //have to find status acc to Order ID & check if complain != 7
                    if ($order->count() > 0) {
                        $status = false;
                        return $this->sendErrorWithData([
                            "status"  => $status,
                            "message" => "Your order is pending. Role not changed successfully.",
                        ], 401, []);
                    }
                }
            } else {
                //find role of user
                $role_user = $this->userRepository->find($request->user_id);
                $input = [];
                //check if role is of customer

                $input['role_id'] = 4;

                //update db
                $updateRole = DB::table('role_user')->where('user_id', $role_user->id)->update($input);
                return $this->sendResponse($updateRole, 'Role changed successfully');
            }
        } elseif ($user->role_id == User::CUSTOMER) {
            $order = $this->orderRepository->findWhere(['user_id' => $request->user_id])->whereNotIn('status',[Order::COMPLETED, Order::PENDING]);

            if ($order->count() > 0) {
                return $this->sendErrorWithData([
                    "status"  => 1,
                    "message" => "Your order is pending. Role not changed successfully.",
                ], 401, []);
            } else {

                //find role of user
                $role_user = $this->userRepository->find($request->user_id);
                $input = [];
                $input['role_id'] = 5;
                //update db
                $updateRole = DB::table('role_user')->where('user_id', $role_user->id)->update($input);
                return $this->sendResponse($updateRole, 'Role changed successfully');

            }
        }

    }

}