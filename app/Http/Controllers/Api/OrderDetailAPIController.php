<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateOrderDetailAPIRequest;
use App\Http\Requests\Api\UpdateOrderDetailAPIRequest;
use App\Models\OrderDetail;
use App\Repositories\Admin\OrderDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class OrderDetailController
 * @package App\Http\Controllers\Api
 */

class OrderDetailAPIController extends AppBaseController
{
    /** @var  OrderDetailRepository */
    private $orderDetailRepository;

    public function __construct(OrderDetailRepository $orderDetailRepo)
    {
        $this->orderDetailRepository = $orderDetailRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/order-details",
     *      summary="Get a listing of the OrderDetails.",
     *      tags={"OrderDetail"},
     *      description="Get all OrderDetails",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OrderDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->orderDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->orderDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $orderDetails = $this->orderDetailRepository->all();

        return $this->sendResponse($orderDetails->toArray(), 'Order Details retrieved successfully');
    }

    /**
     * @param CreateOrderDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/order-details",
     *      summary="Store a newly created OrderDetail in storage",
     *      tags={"OrderDetail"},
     *      description="Store OrderDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOrderDetailAPIRequest $request)
    {
        $orderDetails = $this->orderDetailRepository->saveRecord($request);

        return $this->sendResponse($orderDetails->toArray(), 'Order Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/order-details/{id}",
     *      summary="Display the specified OrderDetail",
     *      tags={"OrderDetail"},
     *      description="Get OrderDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var OrderDetail $orderDetail */
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            return $this->sendError('Order Detail not found');
        }

        return $this->sendResponse($orderDetail->toArray(), 'Order Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrderDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/order-details/{id}",
     *      summary="Update the specified OrderDetail in storage",
     *      tags={"OrderDetail"},
     *      description="Update OrderDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrderDetailAPIRequest $request)
    {
        /** @var OrderDetail $orderDetail */
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            return $this->sendError('Order Detail not found');
        }

        $orderDetail = $this->orderDetailRepository->updateRecord($request, $id);

        return $this->sendResponse($orderDetail->toArray(), 'OrderDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/order-details/{id}",
     *      summary="Remove the specified OrderDetail from storage",
     *      tags={"OrderDetail"},
     *      description="Delete OrderDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OrderDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var OrderDetail $orderDetail */
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            return $this->sendError('Order Detail not found');
        }

        $this->orderDetailRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Order Detail deleted successfully');
    }
}
