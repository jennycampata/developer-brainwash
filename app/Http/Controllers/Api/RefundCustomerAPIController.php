<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateRefundCustomerAPIRequest;
use App\Http\Requests\Api\UpdateRefundCustomerAPIRequest;
use App\Models\RefundCustomer;
use App\Repositories\Admin\RefundCustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class RefundCustomerController
 * @package App\Http\Controllers\Api
 */

class RefundCustomerAPIController extends AppBaseController
{
    /** @var  RefundCustomerRepository */
    private $refundCustomerRepository;

    public function __construct(RefundCustomerRepository $refundCustomerRepo)
    {
        $this->refundCustomerRepository = $refundCustomerRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/refund-customers",
     *      summary="Get a listing of the RefundCustomers.",
     *      tags={"RefundCustomer"},
     *      description="Get all RefundCustomers",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/RefundCustomer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->refundCustomerRepository->pushCriteria(new RequestCriteria($request));
        $this->refundCustomerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $refundCustomers = $this->refundCustomerRepository->all();

        return $this->sendResponse($refundCustomers->toArray(), 'Refund Customers retrieved successfully');
    }

    /**
     * @param CreateRefundCustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/refund-customers",
     *      summary="Store a newly created RefundCustomer in storage",
     *      tags={"RefundCustomer"},
     *      description="Store RefundCustomer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RefundCustomer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RefundCustomer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RefundCustomer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateRefundCustomerAPIRequest $request)
    {
        $refundCustomers = $this->refundCustomerRepository->saveRecord($request);

        return $this->sendResponse($refundCustomers->toArray(), 'Refund Customer saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/refund-customers/{id}",
     *      summary="Display the specified RefundCustomer",
     *      tags={"RefundCustomer"},
     *      description="Get RefundCustomer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RefundCustomer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RefundCustomer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var RefundCustomer $refundCustomer */
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            return $this->sendError('Refund Customer not found');
        }

        return $this->sendResponse($refundCustomer->toArray(), 'Refund Customer retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateRefundCustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/refund-customers/{id}",
     *      summary="Update the specified RefundCustomer in storage",
     *      tags={"RefundCustomer"},
     *      description="Update RefundCustomer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RefundCustomer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="RefundCustomer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/RefundCustomer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/RefundCustomer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateRefundCustomerAPIRequest $request)
    {
        /** @var RefundCustomer $refundCustomer */
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            return $this->sendError('Refund Customer not found');
        }

        $refundCustomer = $this->refundCustomerRepository->updateRecord($request, $id);

        return $this->sendResponse($refundCustomer->toArray(), 'Refund Customer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/refund-customers/{id}",
     *      summary="Remove the specified RefundCustomer from storage",
     *      tags={"RefundCustomer"},
     *      description="Delete RefundCustomer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of RefundCustomer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var RefundCustomer $refundCustomer */
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            return $this->sendError('Refund Customer not found');
        }

        $this->refundCustomerRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Refund Customer deleted successfully');
    }
}
