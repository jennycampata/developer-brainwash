<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateWalletAPIRequest;
use App\Http\Requests\Api\UpdateWalletAPIRequest;
use App\Models\Wallet;
use App\Repositories\Admin\WalletRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class WalletController
 * @package App\Http\Controllers\Api
 */
class WalletAPIController extends AppBaseController
{
    /** @var  WalletRepository */
    private $walletRepository;

    public function __construct(WalletRepository $walletRepo)
    {
        $this->walletRepository = $walletRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallets",
     *      summary="Get a listing of the Wallets.",
     *      tags={"Wallet"},
     *      description="Get all Wallets",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Wallet")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->walletRepository->pushCriteria(new RequestCriteria($request));
        $this->walletRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$wallets = $this->walletRepository->all();

        $user = Auth::id();
        $amount = $this->walletRepository->findWhere(['user_id' => $user])->where('status', '!=', 1);
        $count = 0;
        $var = '';
        foreach ($amount as $item) {
            $count += $item->amount;
        }

        $var = '$' . round($count);
        return $this->sendResponse($var, 'Wallet Amount');
        // return $this->sendResponse($wallets->toArray(), 'Wallets retrieved successfully');
    }

    /**
     * @param CreateWalletAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/wallets",
     *      summary="Store a newly created Wallet in storage",
     *      tags={"Wallet"},
     *      description="Store Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateWalletAPIRequest $request)
    {
        $wallets = $this->walletRepository->saveRecord($request);

        return $this->sendResponse($wallets->toArray(), 'Wallet saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/wallets/{id}",
     *      summary="Display the specified Wallet",
     *      tags={"Wallet"},
     *      description="Get Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Wallet $wallet */
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        return $this->sendResponse($wallet->toArray(), 'Wallet retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateWalletAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/wallets/{id}",
     *      summary="Update the specified Wallet in storage",
     *      tags={"Wallet"},
     *      description="Update Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Wallet that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Wallet")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Wallet"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateWalletAPIRequest $request)
    {
        /** @var Wallet $wallet */
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        $wallet = $this->walletRepository->updateRecord($request, $id);

        return $this->sendResponse($wallet->toArray(), 'Wallet updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/wallets/{id}",
     *      summary="Remove the specified Wallet from storage",
     *      tags={"Wallet"},
     *      description="Delete Wallet",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Wallet",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Wallet $wallet */
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            return $this->sendError('Wallet not found');
        }

        $this->walletRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Wallet deleted successfully');
    }
}
