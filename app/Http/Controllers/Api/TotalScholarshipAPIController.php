<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateTotalScholarshipAPIRequest;
use App\Http\Requests\Api\UpdateTotalScholarshipAPIRequest;
use App\Models\TotalScholarship;
use App\Repositories\Admin\TotalScholarshipRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class TotalScholarshipController
 * @package App\Http\Controllers\Api
 */

class TotalScholarshipAPIController extends AppBaseController
{
    /** @var  TotalScholarshipRepository */
    private $totalScholarshipRepository;

    public function __construct(TotalScholarshipRepository $totalScholarshipRepo)
    {
        $this->totalScholarshipRepository = $totalScholarshipRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/total-scholarships",
     *      summary="Get a listing of the TotalScholarships.",
     *      tags={"TotalScholarship"},
     *      description="Get all TotalScholarships",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/TotalScholarship")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->totalScholarshipRepository->pushCriteria(new RequestCriteria($request));
        $this->totalScholarshipRepository->pushCriteria(new LimitOffsetCriteria($request));
        $totalScholarships = $this->totalScholarshipRepository->all();

        return $this->sendResponse($totalScholarships->toArray(), 'Total Scholarships retrieved successfully');
    }

    /**
     * @param CreateTotalScholarshipAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/total-scholarships",
     *      summary="Store a newly created TotalScholarship in storage",
     *      tags={"TotalScholarship"},
     *      description="Store TotalScholarship",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TotalScholarship that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TotalScholarship")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TotalScholarship"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTotalScholarshipAPIRequest $request)
    {
        $totalScholarships = $this->totalScholarshipRepository->saveRecord($request);

        return $this->sendResponse($totalScholarships->toArray(), 'Total Scholarship saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/total-scholarships/{id}",
     *      summary="Display the specified TotalScholarship",
     *      tags={"TotalScholarship"},
     *      description="Get TotalScholarship",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TotalScholarship",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TotalScholarship"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var TotalScholarship $totalScholarship */
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            return $this->sendError('Total Scholarship not found');
        }

        return $this->sendResponse($totalScholarship->toArray(), 'Total Scholarship retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTotalScholarshipAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/total-scholarships/{id}",
     *      summary="Update the specified TotalScholarship in storage",
     *      tags={"TotalScholarship"},
     *      description="Update TotalScholarship",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TotalScholarship",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="TotalScholarship that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/TotalScholarship")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/TotalScholarship"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTotalScholarshipAPIRequest $request)
    {
        /** @var TotalScholarship $totalScholarship */
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            return $this->sendError('Total Scholarship not found');
        }

        $totalScholarship = $this->totalScholarshipRepository->updateRecord($request, $id);

        return $this->sendResponse($totalScholarship->toArray(), 'TotalScholarship updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/total-scholarships/{id}",
     *      summary="Remove the specified TotalScholarship from storage",
     *      tags={"TotalScholarship"},
     *      description="Delete TotalScholarship",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of TotalScholarship",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var TotalScholarship $totalScholarship */
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            return $this->sendError('Total Scholarship not found');
        }

        $this->totalScholarshipRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Total Scholarship deleted successfully');
    }
}
