<?php

namespace App\Http\Controllers\Api;

use App\Criteria\UserCriteria;
use App\Http\Requests\Api\CreateCancelledOrderAPIRequest;
use App\Http\Requests\Api\UpdateCancelledOrderAPIRequest;
use App\Models\CancelledOrder;
use App\Models\Job;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Wallet;
use App\Repositories\Admin\CancelledOrderRepository;
use App\Repositories\Admin\NotificationRepository;
use App\Repositories\Admin\OrderRepository;
use App\Repositories\Admin\JobRepository;
use App\Repositories\Admin\UserRepository;
use App\Repositories\Admin\WalletRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class CancelledOrderController
 * @package App\Http\Controllers\Api
 */
class CancelledOrderAPIController extends AppBaseController
{
    /** @var  CancelledOrderRepository */
    private $cancelledOrderRepository;

    /** @var  WalletRepository */
    private $walletRepository;

    /** @var  UserRepository */
    private $userRepository;
    private $orderRepository;

    /** @var  JobRepository */
    private $jobRepository;
    private $notificationRepository;

    public function __construct(CancelledOrderRepository $cancelledOrderRepo, UserRepository $userRepo, OrderRepository $orderRepo, WalletRepository $walletRepo, JobRepository $jobRepo, NotificationRepository $notiRepo)
    {
        $this->cancelledOrderRepository = $cancelledOrderRepo;
        $this->orderRepository = $orderRepo;
        $this->userRepository = $userRepo;
        $this->walletRepository = $walletRepo;
        $this->jobRepository = $jobRepo;
        $this->notificationRepository = $notiRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/cancelled-orders",
     *      summary="Get a listing of the CancelledOrders.",
     *      tags={"CancelledOrder"},
     *      description="Get all CancelledOrders",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="orderBy",
     *          description="Pass the property name you want to sort your response. If not found, Returns All Records in DB without sorting.",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="sortedBy",
     *          description="Pass 'asc' or 'desc' to define the sorting method. If not found, 'asc' will be used by default",
     *          type="string",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CancelledOrder")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {


        $this->cancelledOrderRepository->pushCriteria(new RequestCriteria($request));
        $this->cancelledOrderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $cancelledOrders = $this->cancelledOrderRepository->all();

        return $this->sendResponse($cancelledOrders->toArray(), 'Cancelled Orders retrieved successfully');

    }


    /**
     * @param CreateCancelledOrderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/cancelled-orders",
     *      summary="Store a newly created CancelledOrder in storage",
     *      tags={"CancelledOrder"},
     *      description="Store CancelledOrder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CancelledOrder that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CancelledOrder")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CancelledOrder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCancelledOrderAPIRequest $request)
    {

        $id = Auth::id();
        //find the user
        $user = $this->userRepository->find($id);
        $order = $this->orderRepository->find($request->order_id);
        $output = [];
        $output['status'] = Order::PENDING;
        $input = [];
        $input['status'] = 1;
        if ($user->role_id == User::CUSTOMER) {
            if ($request->deduction == 1) {
                $status = [];
                $status['status'] = 1;
                Wallet::where('order_id', $request->order_id)->update($status);
                $amount = $order->details->price - Order::DEDUCTION_AMOUNT;
                $adminAccount = [];
                $washerAccount = [];
                $userAccount = [];

                $adminAccount['user_id'] = User::ADMIN;
                $adminAccount['amount'] = 1;
                $adminAccount['order_id'] = $request->order_id;
                $adminAccount['status'] = 2;

                $this->walletRepository->createNew($adminAccount);

                $washerAccount['user_id'] = $request->washer_id;
                $washerAccount['order_id'] = $request->order_id;
                $washerAccount['status'] = 2;
                $washerAccount['amount'] = 2;

                $this->walletRepository->createNew($washerAccount);

                $userAccount['user_id'] = $request->user_id;
                $userAccount['amount'] = $amount;
                $userAccount['order_id'] = $request->order_id;
                $userAccount['status'] = 2;

                $this->walletRepository->createNew($userAccount);
                $pending = [];
                $pending['status'] = Order::PENDING;
                Order::where('id', $request->order_id)->update($pending);
                $action_type = array_keys(Notification::CANCEL)[0];
                $message = array_values(Notification::CANCEL)[0];
                $notification = [
                    'sender_id'   => Auth::id(),
                    'action_type' => $action_type,
                    'url'         => null,
                    'title'       => array_keys(Notification::TITLE)[0],
                    'ref_id'      => $order->id,
                    'message'     => $message
                ];
                $this->notificationRepository->notification($notification, $order->user_id);

            } elseif ($request->deduction == 0) {
                $wallet = $this->walletRepository->orderBy('id', 'DESC')->findWhere(['order_id' => $request->order_id])->first();
                $this->walletRepository->delete($wallet->id);

            }
        }

        if ($user->role_id == User::WASHER) {
            $wallet = $this->walletRepository->orderBy('id', 'DESC')->findWhere(['order_id' => $request->order_id])->first();
            $this->walletRepository->delete($wallet->id);
        }
        Order::where('id', $request->order_id)->update($output);
        $cancelledOrders = $this->cancelledOrderRepository->saveRecord($request);
        Job::where('order_id', $request->order_id)->update($input);
        $action_type = array_keys(Notification::CANCEL)[0];
        $message = array_values(Notification::CANCEL)[0];
        $notification = [
            'sender_id'   => Auth::id(),
            'action_type' => $action_type,
            'url'         => null,
            'title'       => array_keys(Notification::TITLE)[0],
            'ref_id'      => $request->order_id,
            'message'     => $message
        ];
        $this->notificationRepository->notification($notification, $order->job->user_id);
        return $this->sendResponse($cancelledOrders->toArray(), 'Order Cancelled successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/cancelled-orders/{id}",
     *      summary="Display the specified CancelledOrder",
     *      tags={"CancelledOrder"},
     *      description="Get CancelledOrder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CancelledOrder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CancelledOrder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CancelledOrder $cancelledOrder */
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            return $this->sendError('Cancelled Order not found');
        }

        return $this->sendResponse($cancelledOrder->toArray(), 'Cancelled Order retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCancelledOrderAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/cancelled-orders/{id}",
     *      summary="Update the specified CancelledOrder in storage",
     *      tags={"CancelledOrder"},
     *      description="Update CancelledOrder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CancelledOrder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CancelledOrder that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CancelledOrder")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CancelledOrder"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCancelledOrderAPIRequest $request)
    {
        /** @var CancelledOrder $cancelledOrder */
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            return $this->sendError('Cancelled Order not found');
        }

        $cancelledOrder = $this->cancelledOrderRepository->updateRecord($request, $id);

        return $this->sendResponse($cancelledOrder->toArray(), 'CancelledOrder updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/cancelled-orders/{id}",
     *      summary="Remove the specified CancelledOrder from storage",
     *      tags={"CancelledOrder"},
     *      description="Delete CancelledOrder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CancelledOrder",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CancelledOrder $cancelledOrder */
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            return $this->sendError('Cancelled Order not found');
        }

        $this->cancelledOrderRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Cancelled Order deleted successfully');
    }

    /**
     * @param int $order_id
     * @return Response
     *
     * @SWG\Post(
     *      path="/checkJob",
     *      summary="Check the Customer",
     *      tags={"CancelledOrder"},
     *      description="CancelledOrder",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="order_id",
     *          description="id of CancelledOrder",
     *          type="integer",
     *          required=true,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function checkJob(Request $request)
    {
        $id = Auth::id();
        $user = $this->userRepository->find($id);
        if ($user->role_id == User::CUSTOMER) {
            $data = $this->jobRepository->findWhere(['order_id' => $request->order_id])->pluck('created_at');

            $created_at = Carbon::parse($data[0]);
            $now = Carbon::now();
            $diffMinutes = $created_at->diffInMinutes($now);
            if ($diffMinutes > 3) {
                return $this->sendResponse(true, '$3 will be deducted from your wallet');
            } else {
                return $this->sendResponse(false, 'Cancelled Order successfully');
            }
        }

        return $this->sendResponse(false, 'Cancelled Order successfully');


        //find the user


    }
}
