<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\WasherDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateWasherRequest;
use App\Http\Requests\Admin\UpdateWasherRequest;
use App\Models\UserDetail;
use App\Repositories\Admin\WasherRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class WasherController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  WasherRepository */
    private $washerRepository;

    public function __construct(WasherRepository $washerRepo)
    {
        $this->washerRepository = $washerRepo;
        $this->ModelName = 'washers';
        $this->BreadCrumbName = 'Washers';
    }

    /**
     * Display a listing of the Washer.
     *
     * @param WasherDataTable $washerDataTable
     * @return Response
     */
    public function index(WasherDataTable $washerDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $washerDataTable->render('admin.washers.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Washer.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.washers.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Washer in storage.
     *
     * @param CreateWasherRequest $request
     *
     * @return Response
     */
    public function store(CreateWasherRequest $request)
    {
        $washer = $this->washerRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.washers.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.washers.edit', $washer->id));
        } else {
            $redirect_to = redirect(route('admin.washers.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Washer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.washers.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $washer);
        return view('admin.washers.show')->with(['washer' => $washer, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Washer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.washers.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $washer);
        return view('admin.washers.edit')->with(['washer' => $washer, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Washer in storage.
     *
     * @param  int $id
     * @param UpdateWasherRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWasherRequest $request)
    {
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.washers.index'));
        }

        $washer = $this->washerRepository->updateRecord($request, $washer);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.washers.create'));
        } else {
            $redirect_to = redirect(route('admin.washers.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Washer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $washer = $this->washerRepository->findWithoutFail($id);

        if (empty($washer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.washers.index'));
        }

        $this->washerRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.washers.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
