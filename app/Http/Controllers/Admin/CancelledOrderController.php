<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\CancelledOrderDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateCancelledOrderRequest;
use App\Http\Requests\Admin\UpdateCancelledOrderRequest;
use App\Repositories\Admin\CancelledOrderRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class CancelledOrderController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  CancelledOrderRepository */
    private $cancelledOrderRepository;

    public function __construct(CancelledOrderRepository $cancelledOrderRepo)
    {
        $this->cancelledOrderRepository = $cancelledOrderRepo;
        $this->ModelName = 'cancelled-orders';
        $this->BreadCrumbName = 'Cancelled Orders';
    }

    /**
     * Display a listing of the CancelledOrder.
     *
     * @param CancelledOrderDataTable $cancelledOrderDataTable
     * @return Response
     */
    public function index(CancelledOrderDataTable $cancelledOrderDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $cancelledOrderDataTable->render('admin.cancelled_orders.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new CancelledOrder.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.cancelled_orders.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created CancelledOrder in storage.
     *
     * @param CreateCancelledOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateCancelledOrderRequest $request)
    {
        $cancelledOrder = $this->cancelledOrderRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.cancelled-orders.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.cancelled-orders.edit', $cancelledOrder->id));
        } else {
            $redirect_to = redirect(route('admin.cancelled-orders.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified CancelledOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.cancelled-orders.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $cancelledOrder);
        return view('admin.cancelled_orders.show')->with(['cancelledOrder' => $cancelledOrder, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified CancelledOrder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.cancelled-orders.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $cancelledOrder);
        return view('admin.cancelled_orders.edit')->with(['cancelledOrder' => $cancelledOrder, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified CancelledOrder in storage.
     *
     * @param  int              $id
     * @param UpdateCancelledOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCancelledOrderRequest $request)
    {
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.cancelled-orders.index'));
        }

        $cancelledOrder = $this->cancelledOrderRepository->updateRecord($request, $cancelledOrder);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.cancelled-orders.create'));
        } else {
            $redirect_to = redirect(route('admin.cancelled-orders.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified CancelledOrder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $cancelledOrder = $this->cancelledOrderRepository->findWithoutFail($id);

        if (empty($cancelledOrder)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.cancelled-orders.index'));
        }

        $this->cancelledOrderRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.cancelled-orders.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
