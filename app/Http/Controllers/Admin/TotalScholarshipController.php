<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\TotalScholarshipDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTotalScholarshipRequest;
use App\Http\Requests\Admin\UpdateTotalScholarshipRequest;
use App\Repositories\Admin\TotalScholarshipRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class TotalScholarshipController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  TotalScholarshipRepository */
    private $totalScholarshipRepository;

    public function __construct(TotalScholarshipRepository $totalScholarshipRepo)
    {
        $this->totalScholarshipRepository = $totalScholarshipRepo;
        $this->ModelName = 'total-scholarships';
        $this->BreadCrumbName = 'Total Scholarships';
    }

    /**
     * Display a listing of the TotalScholarship.
     *
     * @param TotalScholarshipDataTable $totalScholarshipDataTable
     * @return Response
     */
    public function index(TotalScholarshipDataTable $totalScholarshipDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $totalScholarshipDataTable->render('admin.total_scholarships.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new TotalScholarship.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.total_scholarships.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created TotalScholarship in storage.
     *
     * @param CreateTotalScholarshipRequest $request
     *
     * @return Response
     */
    public function store(CreateTotalScholarshipRequest $request)
    {
        $totalScholarship = $this->totalScholarshipRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.total-scholarships.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.total-scholarships.edit', $totalScholarship->id));
        } else {
            $redirect_to = redirect(route('admin.total-scholarships.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified TotalScholarship.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.total-scholarships.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $totalScholarship);
        return view('admin.total_scholarships.show')->with(['totalScholarship' => $totalScholarship, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified TotalScholarship.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.total-scholarships.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $totalScholarship);
        return view('admin.total_scholarships.edit')->with(['totalScholarship' => $totalScholarship, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified TotalScholarship in storage.
     *
     * @param  int              $id
     * @param UpdateTotalScholarshipRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTotalScholarshipRequest $request)
    {
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.total-scholarships.index'));
        }

        $totalScholarship = $this->totalScholarshipRepository->updateRecord($request, $totalScholarship);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.total-scholarships.create'));
        } else {
            $redirect_to = redirect(route('admin.total-scholarships.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified TotalScholarship from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $totalScholarship = $this->totalScholarshipRepository->findWithoutFail($id);

        if (empty($totalScholarship)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.total-scholarships.index'));
        }

        $this->totalScholarshipRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.total-scholarships.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
