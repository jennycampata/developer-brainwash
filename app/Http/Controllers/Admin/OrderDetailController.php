<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\OrderDetailDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateOrderDetailRequest;
use App\Http\Requests\Admin\UpdateOrderDetailRequest;
use App\Repositories\Admin\OrderDetailRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class OrderDetailController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  OrderDetailRepository */
    private $orderDetailRepository;

    public function __construct(OrderDetailRepository $orderDetailRepo)
    {
        $this->orderDetailRepository = $orderDetailRepo;
        $this->ModelName = 'order-details';
        $this->BreadCrumbName = 'Order Details';
    }

    /**
     * Display a listing of the OrderDetail.
     *
     * @param OrderDetailDataTable $orderDetailDataTable
     * @return Response
     */
    public function index(OrderDetailDataTable $orderDetailDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $orderDetailDataTable->render('admin.order_details.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new OrderDetail.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.order_details.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created OrderDetail in storage.
     *
     * @param CreateOrderDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderDetailRequest $request)
    {
        $orderDetail = $this->orderDetailRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.order-details.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.order-details.edit', $orderDetail->id));
        } else {
            $redirect_to = redirect(route('admin.order-details.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified OrderDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.order-details.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $orderDetail);
        return view('admin.order_details.show')->with(['orderDetail' => $orderDetail, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified OrderDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.order-details.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $orderDetail);
        return view('admin.order_details.edit')->with(['orderDetail' => $orderDetail, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified OrderDetail in storage.
     *
     * @param  int              $id
     * @param UpdateOrderDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderDetailRequest $request)
    {
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.order-details.index'));
        }

        $orderDetail = $this->orderDetailRepository->updateRecord($request, $orderDetail);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.order-details.create'));
        } else {
            $redirect_to = redirect(route('admin.order-details.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified OrderDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderDetail = $this->orderDetailRepository->findWithoutFail($id);

        if (empty($orderDetail)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.order-details.index'));
        }

        $this->orderDetailRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.order-details.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
