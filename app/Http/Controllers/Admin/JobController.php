<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\JobDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateJobRequest;
use App\Http\Requests\Admin\UpdateJobRequest;
use App\Models\Job;
use App\Models\Order;
use App\Repositories\Admin\JobRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class JobController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  JobRepository */
    private $jobRepository;

    public function __construct(JobRepository $jobRepo)
    {
        $this->jobRepository = $jobRepo;
        $this->ModelName = 'jobs';
        $this->BreadCrumbName = 'Jobs';
    }

    /**
     * Display a listing of the Job.
     *
     * @param JobDataTable $jobDataTable
     * @return Response
     */
    public function index(JobDataTable $jobDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $jobDataTable->render('admin.jobs.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Job.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.jobs.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Job in storage.
     *
     * @param CreateJobRequest $request
     *
     * @return Response
     */
    public function store(CreateJobRequest $request)
    {
        $job = $this->jobRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.jobs.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.jobs.edit', $job->id));
        } else {
            $redirect_to = redirect(route('admin.jobs.index'));
        }

        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.jobs.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $job);
        return view('admin.jobs.show')->with(['job' => $job, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.jobs.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $job);
        return view('admin.jobs.edit')->with(['job' => $job, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Job in storage.
     *
     * @param  int $id
     * @param UpdateJobRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobRequest $request)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.jobs.index'));
        }

        $job = $this->jobRepository->updateRecord($request, $job);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.jobs.create'));
        } else {
            $redirect_to = redirect(route('admin.jobs.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Job from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.jobs.index'));
        }

        $this->jobRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.jobs.index'))->with(['title' => $this->BreadCrumbName]);
    }

    public function toggleBtn(Request $request)
    {

        if ($request->id) {
            $job = $this->jobRepository->find($request->id);
            if ($job->status == 1) {
                $input['status'] = 0;
                Flash::success('Job Activated successfully.');
            } else {
                $input['status'] = 1;
                Flash::success('Job Blocked successfully.');
            }

            Job::where('id', $job->id)->update($input);
            return redirect()->back();
        }

    }
}
