<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\WalletDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateWalletRequest;
use App\Http\Requests\Admin\UpdateWalletRequest;
use App\Repositories\Admin\WalletRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class WalletController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  WalletRepository */
    private $walletRepository;

    public function __construct(WalletRepository $walletRepo)
    {
        $this->walletRepository = $walletRepo;
        $this->ModelName = 'wallets';
        $this->BreadCrumbName = 'Wallets';
    }

    /**
     * Display a listing of the Wallet.
     *
     * @param WalletDataTable $walletDataTable
     * @return Response
     */
    public function index(WalletDataTable $walletDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $walletDataTable->render('admin.wallets.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Wallet.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.wallets.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Wallet in storage.
     *
     * @param CreateWalletRequest $request
     *
     * @return Response
     */
    public function store(CreateWalletRequest $request)
    {
        $wallet = $this->walletRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.wallets.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.wallets.edit', $wallet->id));
        } else {
            $redirect_to = redirect(route('admin.wallets.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Wallet.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.wallets.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $wallet);
        return view('admin.wallets.show')->with(['wallet' => $wallet, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Wallet.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.wallets.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $wallet);
        return view('admin.wallets.edit')->with(['wallet' => $wallet, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Wallet in storage.
     *
     * @param  int              $id
     * @param UpdateWalletRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWalletRequest $request)
    {
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.wallets.index'));
        }

        $wallet = $this->walletRepository->updateRecord($request, $wallet);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.wallets.create'));
        } else {
            $redirect_to = redirect(route('admin.wallets.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Wallet from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wallet = $this->walletRepository->findWithoutFail($id);

        if (empty($wallet)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.wallets.index'));
        }

        $this->walletRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.wallets.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
