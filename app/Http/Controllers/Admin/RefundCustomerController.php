<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\RefundCustomerDataTable;
use App\Http\Requests\Admin\CreateRefundCustomerRequest;
use App\Http\Requests\Admin\UpdateRefundCustomerRequest;
use App\Models\RefundCustomer;
use App\Repositories\Admin\RefundCustomerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

use PayPal\Api\Amount;
use PayPal\Api\Refund;
use PayPal\Api\RefundRequest;
use PayPal\Api\Sale;
use PayPal\Exception\PayPalConnectionException;

class RefundCustomerController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  RefundCustomerRepository */
    private $refundCustomerRepository;

    public function __construct(RefundCustomerRepository $refundCustomerRepo)
    {
        $this->refundCustomerRepository = $refundCustomerRepo;
        $this->ModelName = 'refund-customers';
        $this->BreadCrumbName = 'Refund Customers';
    }

    /**
     * Display a listing of the RefundCustomer.
     *
     * @param RefundCustomerDataTable $refundCustomerDataTable
     * @return Response
     */
    public function index(RefundCustomerDataTable $refundCustomerDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $refundCustomerDataTable->render('admin.refund_customers.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new RefundCustomer.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.refund_customers.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created RefundCustomer in storage.
     *
     * @param CreateRefundCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateRefundCustomerRequest $request)
    {
        $refundCustomer = $this->refundCustomerRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.refund-customers.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.refund-customers.edit', $refundCustomer->id));
        } else {
            $redirect_to = redirect(route('admin.refund-customers.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified RefundCustomer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.refund-customers.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $refundCustomer);
        return view('admin.refund_customers.show')->with(['refundCustomer' => $refundCustomer, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified RefundCustomer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.refund-customers.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $refundCustomer);
        return view('admin.refund_customers.edit')->with(['refundCustomer' => $refundCustomer, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified RefundCustomer in storage.
     *
     * @param  int $id
     * @param UpdateRefundCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRefundCustomerRequest $request)
    {
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.refund-customers.index'));
        }

        $refundCustomer = $this->refundCustomerRepository->updateRecord($request, $refundCustomer);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.refund-customers.create'));
        } else {
            $redirect_to = redirect(route('admin.refund-customers.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified RefundCustomer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $refundCustomer = $this->refundCustomerRepository->findWithoutFail($id);

        if (empty($refundCustomer)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.refund-customers.index'));
        }

        $this->refundCustomerRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.refund-customers.index'))->with(['title' => $this->BreadCrumbName]);
    }

    public function toggleBtn(Request $request)
    {
        if ($request->id) {
            $refund = $this->refundCustomerRepository->find($request->id);
            if ($refund->status == 1) {
                $input['status'] = 0;
                Flash::success('University Activated successfully.');
            } else {
                $input['status'] = 1;
                Flash::success('University Blocked successfully.');
            }

            RefundCustomer::where('id', $refund->id)->update($input);
            return redirect()->back();
        }
    }

    public function refund(Request $request)
    {
//        dd($request);
        $amt = new Amount();
        //$amt = $request->get('amount');

        $amt->setTotal(19.98)
            ->setCurrency('USD');

        $refund = new RefundRequest();
        $refund->setAmount($amt);

//        $sale = new Sale();
//        $sale->setId("PAYID-LXP6YQQ4DS316267D8405024");
        //psyment ID mentioned above
//        $sale->setId($saleId);
        try {
            $apiContext =
                new \PayPal\Rest\ApiContext(
                    new \PayPal\Auth\OAuthTokenCredential(
                        env('PAYPAL_CLIENT_ID', ''),
                        env('PAYPAL_SECRET', '')
                    )
                );
//            $refundedSale = $sale->refundSale($refund, $apiContext);
        }catch (PayPalConnectionException $e) {
            echo $e->getData(); // This will print a JSON which has specific details about the error.
            exit;
        }

        return 'Successful';
    }
}
