<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\UniversityDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateUniversityRequest;
use App\Http\Requests\Admin\UpdateUniversityRequest;
use App\Models\University;
use App\Repositories\Admin\UniversityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class UniversityController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  UniversityRepository */
    private $universityRepository;

    public function __construct(UniversityRepository $universityRepo)
    {
        $this->universityRepository = $universityRepo;
        $this->ModelName = 'universities';
        $this->BreadCrumbName = 'Universities';
    }

    /**
     * Display a listing of the University.
     *
     * @param UniversityDataTable $universityDataTable
     * @return Response
     */
    public function index(UniversityDataTable $universityDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $universityDataTable->render('admin.universities.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new University.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.universities.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created University in storage.
     *
     * @param CreateUniversityRequest $request
     *
     * @return Response
     */
    public function store(CreateUniversityRequest $request)
    {
        $university = $this->universityRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.universities.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.universities.edit', $university->id));
        } else {
            $redirect_to = redirect(route('admin.universities.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified University.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $university = $this->universityRepository->findWithoutFail($id);

        if (empty($university)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.universities.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $university);
        return view('admin.universities.show')->with(['university' => $university, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified University.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $university = $this->universityRepository->findWithoutFail($id);

        if (empty($university)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.universities.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $university);
        return view('admin.universities.edit')->with(['university' => $university, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified University in storage.
     *
     * @param  int $id
     * @param UpdateUniversityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUniversityRequest $request)
    {
        $university = $this->universityRepository->findWithoutFail($id);

        if (empty($university)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.universities.index'));
        }

        $university = $this->universityRepository->updateRecord($request, $university);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.universities.create'));
        } else {
            $redirect_to = redirect(route('admin.universities.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified University from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $university = $this->universityRepository->findWithoutFail($id);

        if (empty($university)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.universities.index'));
        }

        $this->universityRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.universities.index'))->with(['title' => $this->BreadCrumbName]);
    }

    public function toggleBtn(Request $request)
    {
        if ($request->id) {
            $uni = $this->universityRepository->find($request->id);
            if ($uni->status == 1) {
                $input['status'] = 0;
                Flash::success('University Activated successfully.');
            } else {
                $input['status'] = 1;
                Flash::success('University Blocked successfully.');
            }

            University::where('id', $uni->id)->update($input);
            return redirect()->back();
        }

    }
}
