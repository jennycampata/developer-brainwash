<?php

namespace App\Http\Requests\Api;

use App\Models\OrderDetail;

class UpdateOrderDetailAPIRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return OrderDetail::$api_update_rules;
    }
}
