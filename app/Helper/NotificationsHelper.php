<?php

namespace App\Helper;

use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Facades\Config;

class NotificationsHelper
{
    function sendPushNotifications($msg = '', $deviceObject = [], $extraPayLoadData = [], $title = '', $refid = '', $actiontype = '')
    {
        $androidDeviceToken = [];
        $iosDeviceToken = [];

        foreach ($deviceObject as $device):
            if (strtolower($device['device_type']) == 'android') {
                $androidDeviceToken[] = $device['device_token'];
            } elseif (strtolower($device['device_type']) == 'ios') {
                $iosDeviceToken[] = $device['device_token'];
            }
        endforeach;


        if ($androidDeviceToken) {
            $push = new PushNotification('fcm');
            $data = [
                'title' => $title,
                'body'  => $msg,
                'ref_id' => $refid,
                'action_type' => $actiontype
            ];
            $notif = [
                'ref_id' => $refid,
                'action_type' => $actiontype
            ];

            $android = $push->setMessage([
                'notification' => $data,
                'data' => $notif
            ])->setApiKey(Config::get('pushnotification.fcm.apiKey'))
                ->setConfig(['dry_run' => false])
                ->setDevicesToken($androidDeviceToken)
                ->send();

        }
        if ($iosDeviceToken) {
            $push = new PushNotification('fcm');
            $data = [
                'title' => $title,
                'body'  => $msg,
                'sound' => 'default',
                'ref_id' => $refid,
                'action_type' => $actiontype
            ];
            $notif = [
                'ref_id' => $refid,
                'action_type' => $actiontype
            ];

            $push->setMessage([
                'notification' => $data,
                'data' => $notif
            ])->setApiKey(Config::get('pushnotification.fcm.apiKey'))
                ->setConfig(['dry_run' => false])
                ->setDevicesToken($iosDeviceToken)
                ->send();
        }

        return true;
    }
}


