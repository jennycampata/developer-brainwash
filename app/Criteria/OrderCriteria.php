<?php

namespace App\Criteria;

use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OrderCriteria.
 *
 * @package namespace App\Criteria;
 */
class OrderCriteria extends BaseCriteria implements CriteriaInterface
{

    protected $up_latitude = null;
    protected $up_longitude = null;
    protected $radius = 2;

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */

    public function apply($model, RepositoryInterface $repository)
    {
        //20 locations within 25 miles, Put 3959 for miles.
        //20 locations within 40.2 kms, Put 6371 for kms.
        //for 2 miles = 263.933
        $model = DB::table("orders")->select("*",
            DB::raw("263.933 * acos(cos(radians(" . $this->up_latitude . "))
        * cos(radians(orders.up_latitude))
        * cos(radians(orders.up_longitude) - radians(" . $this->up_longitude . "))
        + sin(radians(" . $this->up_latitude . "))
        * sin(radians(orders.up_latitude))) AS distance"))
            ->having('distance', '<', $this->radius)
            ->orderBy('distance')
            ->join('order_details', function ($join) {
                $join->on('orders.id', '=', 'order_details.order_id');
            })->get();

//        var_dump($model);
//        exit();

        return $model;
    }
}


// https://www.itsolutionstuff.com/post/how-to-find-nearest-location-using-latitude-and-longitude-in-laravelexample.html