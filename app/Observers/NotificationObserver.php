<?php

namespace App\Observers;

use App\Jobs\SendPushNotification;
use App\Models\NotificationUser;

class NotificationObserver
{
    /**
     * @param NotificationUser $notificationUser
     */
    public function created(NotificationUser $notificationUser)
    {
        $message = $notificationUser->notification->message;
        $deviceData = $notificationUser->user->devices;
        $title = $notificationUser->notification->title;
        $ref_id = $notificationUser->notification->ref_id;
        $action_type = $notificationUser->notification->action_type;
        $job = new SendPushNotification($message, $deviceData, '', $title, $ref_id, $action_type);
        dispatch($job);
    }
}
