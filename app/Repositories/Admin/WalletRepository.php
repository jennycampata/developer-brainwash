<?php

namespace App\Repositories\Admin;

use App\Models\Wallet;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class WalletRepository
 * @package App\Repositories\Admin
 * @version December 16, 2019, 10:49 am UTC
 *
 * @method Wallet findWithoutFail($id, $columns = ['*'])
 * @method Wallet find($id, $columns = ['*'])
 * @method Wallet first($columns = ['*'])
 */
class WalletRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Wallet::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $wallet = $this->create($input);
        return $wallet;
    }

    /**
     * @param $request
     * @param $wallet
     * @return mixed
     */
    public function updateRecord($request, $wallet)
    {
        $input = $request->all();
        $wallet = $this->update($input, $wallet->id);
        return $wallet;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $wallet = $this->delete($id);
        return $wallet;
    }

    public function createNew($input)
    {
        $washer = $this->create($input);
        return $washer;
    }
}
