<?php

namespace App\Repositories\Admin;

use App\Models\TotalScholarship;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TotalScholarshipRepository
 * @package App\Repositories\Admin
 * @version November 13, 2019, 11:07 am UTC
 *
 * @method TotalScholarship findWithoutFail($id, $columns = ['*'])
 * @method TotalScholarship find($id, $columns = ['*'])
 * @method TotalScholarship first($columns = ['*'])
*/
class TotalScholarshipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'university_id',
        'total_raised'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TotalScholarship::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $totalScholarship = $this->create($input);
        return $totalScholarship;
    }

    /**
     * @param $request
     * @param $totalScholarship
     * @return mixed
     */
    public function updateRecord($request, $totalScholarship)
    {
        $input = $request->all();
        $totalScholarship = $this->update($input, $totalScholarship->id);
        return $totalScholarship;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $totalScholarship = $this->delete($id);
        return $totalScholarship;
    }
}
