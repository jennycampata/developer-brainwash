<?php

namespace App\Repositories\Admin;

use App\Models\Order;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrderRepository
 * @package App\Repositories\Admin
 * @version November 4, 2019, 2:31 pm UTC
 *
 * @method Order findWithoutFail($id, $columns = ['*'])
 * @method Order find($id, $columns = ['*'])
 * @method Order first($columns = ['*'])
 */
class OrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'up_latitude',
        'up_longitude',
        'down_latitude',
        'down_longitude',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Order::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $order = $this->create($input);
        return $order;
    }

    /**
     * @param $request
     * @param $order
     * @return mixed
     */
    public function updateRecord($request, $order)
    {
        $input = $request->all();
        $order = $this->update($input, $order->id);
        return $order;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $order = $this->delete($id);
        return $order;
    }

    public function updateStatus($input, $id)
    {
        $order = $this->update($input, $id);
        return $order;
    }
}
