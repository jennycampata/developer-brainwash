<?php

namespace App\Repositories\Admin;

use App\Models\Job;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JobRepository
 * @package App\Repositories\Admin
 * @version November 7, 2019, 2:25 pm UTC
 *
 * @method Job findWithoutFail($id, $columns = ['*'])
 * @method Job find($id, $columns = ['*'])
 * @method Job first($columns = ['*'])
*/
class JobRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'order_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Job::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $job = $this->create($input);
        return $job;
    }

    /**
     * @param $request
     * @param $job
     * @return mixed
     */
    public function updateRecord($request, $job)
    {
        $input = $request->all();
        $job = $this->update($input, $job->id);
        return $job;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $job = $this->delete($id);
        return $job;
    }

    public function updateStatus($request, $job){
        $job = $this->update($request, $job->id);
        return $job;
    }
}
