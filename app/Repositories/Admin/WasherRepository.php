<?php

namespace App\Repositories\Admin;

use App\Models\Washer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class WasherRepository
 * @package App\Repositories\Admin
 * @version November 1, 2019, 11:11 am UTC
 *
 * @method Washer findWithoutFail($id, $columns = ['*'])
 * @method Washer find($id, $columns = ['*'])
 * @method Washer first($columns = ['*'])
 */
class WasherRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Washer::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $washer = $this->create($input);
        return $washer;
    }

    /**
     * @param $request
     * @param $washer
     * @return mixed
     */
    public function updateRecord($request, $washer)
    {
        $input = $request->all();
        $washer = $this->update($input, $washer->id);
        return $washer;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $washer = $this->delete($id);
        return $washer;
    }


}
