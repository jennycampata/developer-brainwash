<?php

namespace App\Repositories\Admin;

use App\Models\Transaction;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TransactionRepository
 * @package App\Repositories\Admin
 * @version November 4, 2019, 2:29 pm UTC
 *
 * @method Transaction findWithoutFail($id, $columns = ['*'])
 * @method Transaction find($id, $columns = ['*'])
 * @method Transaction first($columns = ['*'])
*/
class TransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'user_id',
        'payment_id',
        'payment_method',
        'details',
        'state',
        'amount',
        'create_time'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transaction::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $transaction = $this->create($input);
        return $transaction;
    }

    /**
     * @param $request
     * @param $transaction
     * @return mixed
     */
    public function updateRecord($request, $transaction)
    {
        $input = $request->all();
        $transaction = $this->update($input, $transaction->id);
        return $transaction;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $transaction = $this->delete($id);
        return $transaction;
    }
}
