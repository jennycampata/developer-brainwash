<?php

namespace App\Repositories\Admin;

use App\Models\RefundCustomer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RefundCustomerRepository
 * @package App\Repositories\Admin
 * @version November 19, 2019, 7:29 am UTC
 *
 * @method RefundCustomer findWithoutFail($id, $columns = ['*'])
 * @method RefundCustomer find($id, $columns = ['*'])
 * @method RefundCustomer first($columns = ['*'])
*/
class RefundCustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'university_id',
        'amount',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RefundCustomer::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $refundCustomer = $this->create($input);
        return $refundCustomer;
    }

    /**
     * @param $request
     * @param $refundCustomer
     * @return mixed
     */
    public function updateRecord($request, $refundCustomer)
    {
        $input = $request->all();
        $refundCustomer = $this->update($input, $refundCustomer->id);
        return $refundCustomer;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $refundCustomer = $this->delete($id);
        return $refundCustomer;
    }
}
