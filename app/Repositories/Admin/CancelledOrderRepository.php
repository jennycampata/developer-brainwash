<?php

namespace App\Repositories\Admin;

use App\Models\CancelledOrder;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CancelledOrderRepository
 * @package App\Repositories\Admin
 * @version December 11, 2019, 3:35 pm UTC
 *
 * @method CancelledOrder findWithoutFail($id, $columns = ['*'])
 * @method CancelledOrder find($id, $columns = ['*'])
 * @method CancelledOrder first($columns = ['*'])
*/
class CancelledOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'order_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CancelledOrder::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $cancelledOrder = $this->create($input);
        return $cancelledOrder;
    }

    /**
     * @param $request
     * @param $cancelledOrder
     * @return mixed
     */
    public function updateRecord($request, $cancelledOrder)
    {
        $input = $request->all();
        $cancelledOrder = $this->update($input, $cancelledOrder->id);
        return $cancelledOrder;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $cancelledOrder = $this->delete($id);
        return $cancelledOrder;
    }
}
