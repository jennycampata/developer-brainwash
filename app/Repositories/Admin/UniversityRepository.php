<?php

namespace App\Repositories\Admin;

use App\Models\University;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UniversityRepository
 * @package App\Repositories\Admin
 * @version November 4, 2019, 2:33 pm UTC
 *
 * @method University findWithoutFail($id, $columns = ['*'])
 * @method University find($id, $columns = ['*'])
 * @method University first($columns = ['*'])
*/
class UniversityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return University::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $university = $this->create($input);
        return $university;
    }

    /**
     * @param $request
     * @param $university
     * @return mixed
     */
    public function updateRecord($request, $university)
    {
        $input = $request->all();
        $university = $this->update($input, $university->id);
        return $university;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $university = $this->delete($id);
        return $university;
    }
}
