<?php

namespace App\Repositories\Admin;

use App\Models\Notification;
use App\Models\NotificationUser;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class NotificationRepository
 * @package App\Repositories\Admin
 * @version July 14, 2018, 6:54 am UTC
 *
 * @method Notification findWithoutFail($id, $columns = ['*'])
 * @method Notification find($id, $columns = ['*'])
 * @method Notification first($columns = ['*'])
 */
class NotificationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'url',
        'action_type',
        'ref_id',
        'message',
        'created_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notification::class;
    }

    public function notification($orderNotification, $receiver_id)
    {
        $notification = $this->create($orderNotification);
        NotificationUser::create([
            'notification_id' => $notification->id,
            'user_id'         => $receiver_id,
            'status'          => 10
        ]);
        return true;
    }
}
