<?php

namespace App\Repositories\Admin;

use App\Models\Report;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReportRepository
 * @package App\Repositories\Admin
 * @version November 7, 2019, 7:26 am UTC
 *
 * @method Report findWithoutFail($id, $columns = ['*'])
 * @method Report find($id, $columns = ['*'])
 * @method Report first($columns = ['*'])
*/
class ReportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'role_id',
        'reason',
        'block',
        'refund',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Report::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $report = $this->create($input);
        return $report;
    }

    /**
     * @param $request
     * @param $report
     * @return mixed
     */
    public function updateRecord($request, $report)
    {
        $input = $request->all();
        $report = $this->update($input, $report->id);
        return $report;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $report = $this->delete($id);
        return $report;
    }
}
