<?php

namespace App\Repositories\Admin;

use App\Models\OrderDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class OrderDetailRepository
 * @package App\Repositories\Admin
 * @version November 4, 2019, 2:30 pm UTC
 *
 * @method OrderDetail findWithoutFail($id, $columns = ['*'])
 * @method OrderDetail find($id, $columns = ['*'])
 * @method OrderDetail first($columns = ['*'])
*/
class OrderDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'no_bags',
        'type',
        'detergent',
        'folded',
        'hung',
        'instruction',
        'price'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderDetail::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $orderDetail = $this->create($input);
        return $orderDetail;
    }

    /**
     * @param $request
     * @param $orderDetail
     * @return mixed
     */
    public function updateRecord($request, $orderDetail)
    {
        $input = $request->all();
        $orderDetail = $this->update($input, $orderDetail->id);
        return $orderDetail;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $orderDetail = $this->delete($id);
        return $orderDetail;
    }
}
