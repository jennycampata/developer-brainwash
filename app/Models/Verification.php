<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property integer user_id
 * @property integer code
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Verification",
 *       required={"user_id", "code"},
 *    @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="integer"
 *      ),@SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="integer"
 *      ),@SWG\Property(
 *          property="code",
 *          description="code",
 *          type="integer",
 *          format="integer"
 *      )
 * )
 */
class Verification extends Model
{
    use SoftDeletes;
    public $table = 'verification';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'code', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [
    ];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [
    ];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'user_id',
        'code'
    ];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'code'    => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
