<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Report",
 *      required={"user_id", "role_id", "reason", "block", "refund", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reported_from",
 *          description="reported_from",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reported_to",
 *          description="reported_to",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reason",
 *          description="reason",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="refund",
 *          description="refund",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Report extends Model
{
    use SoftDeletes;

    public $table = 'reports';

    const RESOLVED = 0;
    const UNRESOLVED = 1;
    const BLOCKED = 2;

    public static $dropdown = [
        self::RESOLVED   => 'Resolved',
        self::UNRESOLVED => 'Un Resolved',
        self::BLOCKED    => 'Blocked'
    ];


    protected $dates = ['deleted_at'];


    public $fillable = [
        'reported_to',
        'reported_from',
        'reason',
        'refund',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'reported_to' => 'integer',
        'reported_from' => 'integer',
        'reason'      => 'string',
        'refund'      => 'integer',
        'status'      => 'boolean'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'reported_to' => 'required',
        'reported_from' => 'required',
        'reason'      => 'required',
//        'refund'      => 'required',
        'status'      => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'reported_to' => 'required',
        'reported_from' => 'required',
        'reason'      => 'required',
//        'refund'      => 'required',
        'status'      => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'reported_to' => 'required',
        'reported_from' => 'required',
        'reason'      => 'required',
//        'refund'      => 'required',
        'status'      => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'reported_to' => 'required',
        'reported_from' => 'required',
        'reason'      => 'required',
//        'refund'      => 'required',
        'status'      => 'required'
    ];

    public function reportedTo()
    {
        return $this->belongsTo(User::class, 'reported_to');
    }

    public function reportedBy()
    {
        return $this->belongsTo(User::class, 'reported_from');
    }

}
