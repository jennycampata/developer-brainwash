<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="OrderDetail",
 *      required={"order_id", "no_bags", "type", "detergent", "folded", "hung", "instruction", "price", "donation"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_id",
 *          description="order_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="no_bags",
 *          description="no_bags",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="detergent",
 *          description="detergent",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="folded",
 *          description="folded",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="hung",
 *          description="hung",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="instruction",
 *          description="instruction",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="donation",
 *          description="donation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class OrderDetail extends Model
{
    use SoftDeletes;

    public $table = 'order_details';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'no_bags',
        'type',
        'detergent',
        'folded',
        'hung',
        'instruction',
        'price',
        'donation'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'order_id'    => 'integer',
        'no_bags'     => 'integer',
        'type'        => 'boolean',
        'detergent'   => 'boolean',
        'folded'      => 'boolean',
        'hung'        => 'boolean',
        'instruction' => 'string',
        'price'       => 'string',
        'donation'    => 'string'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'order_id'    => 'required',
        'no_bags'     => 'required',
        'type'        => 'required',
        'detergent'   => 'required',
        'folded'      => 'required',
        'hung'        => 'required',
        'instruction' => 'required',
        'price'       => 'required',
        'donation'    => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'order_id'    => 'required',
        'no_bags'     => 'required',
        'type'        => 'required',
        'detergent'   => 'required',
        'folded'      => 'required',
        'hung'        => 'required',
        'instruction' => 'required',
        'price'       => 'required',
        'donation'    => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'order_id'    => 'required',
        'no_bags'     => 'required',
        'type'        => 'required',
        'detergent'   => 'required',
        'folded'      => 'required',
        'hung'        => 'required',
        'instruction' => 'required',
        'price'       => 'required',
        'donation'    => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'order_id'    => 'required',
        'no_bags'     => 'required',
        'type'        => 'required',
        'detergent'   => 'required',
        'folded'      => 'required',
        'hung'        => 'required',
        'instruction' => 'required',
        'price'       => 'required',
        'donation'    => 'required'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
