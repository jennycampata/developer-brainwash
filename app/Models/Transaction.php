<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={"id", "user_id", "order_id", "create_time", "details", "payment_id", "payment_method", "state", "amount"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_id",
 *          description="id",
 *          type="string"
 *      ),
 *       @SWG\Property(
 *          property="user_id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *       @SWG\Property(
 *          property="order_id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_method",
 *          description="payment method",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="details",
 *          description=" details",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="state",
 *          description="state",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="create_time",
 *          description="create_time",
 *          type="string",
 *          format="date-time",
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *     @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Transaction extends Model
{
    use SoftDeletes;

    public $table = 'transactions';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'user_id',
        'payment_id',
        'payment_method',
        'details',
        'create_time',
        'amount',
        'state'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'order_id'       => 'integer',
        'user_id'        => 'integer',
        'payment_id'     => 'string',
        'payment_method' => 'string',
        'create_time'    => 'string',
        'details'        => 'string',
        'amount'         => 'integer',
        'state'          => 'string'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'order_id'       => 'required',
        'user_id'        => 'required',
        'payment_id'     => 'required',
//        'create_time'    => 'required',
//        'details'        => 'required',
        'payment_method' => 'required',
        'amount'         => 'required',
        'state'          => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'order_id'       => 'required',
        'user_id'        => 'required',
        'payment_id'     => 'required',
//        'create_time'    => 'required',
//        'details'        => 'required',
        'payment_method' => 'required',
        'amount'         => 'required',
        'state'          => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'order_id'       => 'required',
        'user_id'        => 'required',
        'payment_id'     => 'required',
        'create_time'    => 'required',
//        'details'        => 'required',
        'payment_method' => 'required',
        'amount'         => 'required',
        'state'          => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'order_id'       => 'required',
        'user_id'        => 'required',
        'payment_id'     => 'required',
        'details'        => 'required',
        'create_time'    => 'required',
        'payment_method' => 'required',
        'amount'         => 'required',
        'state'          => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
