<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="University",
 *      required={"name", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Scholarships",
 *      required={"name", "total_raised"},
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="total_raised",
 *          description="total_raised",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */

class University extends Model
{
    use SoftDeletes;

    public $table = 'universities';


    const ACTIVE = 1;
    const BLOCKED = 0;

    public static $status = [
        self::ACTIVE    => 'Active',
        self::BLOCKED   => 'Blocked'
];

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'     => 'integer',
        'name'   => 'string',
        'status' => 'boolean'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [
    ];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [
        'total_amount',
        'uni_name'
    ];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'name'   => 'required',
        'status' => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'name'   => 'required',
        'status' => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'name'   => 'required',
        'status' => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'name'   => 'required',
        'status' => 'required'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function user_university()
    {
        return $this->hasOne(UserUniversity::class, 'university_id');
    }

    public function scholarships(){
        return $this->hasMany(TotalScholarship::class, 'university_id');
    }

    public function refund(){
        return $this->hasMany(RefundCustomer::class, 'university_id');
    }

    public function getTotalAmountAttribute(){
        $total = TotalScholarship::where('university_id', $this->id)->pluck('total_raised');
        $count = 0;
        foreach ($total as $t){
            $count += $t;
        }
        return $count;
    }

    public function getUniNameAttribute(){
        $user = Auth::id();
        $uu = UserUniversity::all()->where('user_id', '=', $user)->pluck('university_id')->first();
//        $uu = UserUniversity::all()->where('user_id', $this->id)->pluck('university_id');
        return $uu;
    }
}
