<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Order",
 *      required={"user_id", "up_latitude", "up_longitude", "pick_up", "down_latitude", "down_longitude", "drop_off", "status", "no_bags", "type", "detergent", "folded", "hung", "instruction", "price", "donation"},
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="up_latitude",
 *          description="up_latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="up_longitude",
 *          description="up_longitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="down_latitude",
 *          description="down_latitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="down_longitude",
 *          description="down_longitude",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pick_up",
 *          description="pick_up",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="drop_off",
 *          description="drop_off",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="no_bags",
 *          description="no_bags",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="detergent",
 *          description="detergent",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="folded",
 *          description="folded",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="hung",
 *          description="hung",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="instruction",
 *          description="instruction",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="donation",
 *          description="donation",
 *          type="string"
 *      )
 * )
 */

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="FindWash",
 *      required={"latitude", "longitude"},
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="string",
 *          format="string"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="string"
 *      )
 * )
 */

/**
 * @property integer id
 * @property string user_id
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="routing",
 *      required={"user_id"},
 *      @SWG\Property(
 *          property="user_id",
 *          description="Id of User",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */

/**
 * @property integer id
 * @property integer user_id
 * @property integer amount
 *
 * @SWG\Definition(
 *      definition="wallet"
 * )
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';

    // ORDER STATUS
    const DEDUCTION_AMOUNT = 3;
    const NEW = 0;
    const ACCEPT = 1; //ACCEPTED BY WASHER
    const PICKUP = 2;
    const WASH = 3;
    const DRIER = 4;
    const FOLDING = 5;
    const DROPOFF = 6;
    const COMPLETED = 7;
    const CANCELLED = 8;
    const PENDING = 9;

    public static $status = [
        self::ACCEPT    => 'Pending',
        self::NEW       => 'New',
        self::PICKUP    => 'Pickup',
        self::WASH      => 'Washer',
        self::DRIER     => 'Drier',
        self::FOLDING   => 'Folding',
        self::DROPOFF   => 'Dropoff',
        self::COMPLETED => 'Completed'
    ];

    protected $dates = ['deleted_at'];

    public $fillable = [
        'id',
        'user_id',
        'up_latitude',
        'status',
        'up_longitude',
        'pick_up',
        'down_latitude',
        'down_longitude',
        'drop_off',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'             => 'integer',
        'user_id'        => 'integer',
        'up_latitude'    => 'string',
        'up_longitude'   => 'string',
        'pick_up'        => 'string',
        'drop_off'       => 'string',
        'down_latitude'  => 'string',
        'down_longitude' => 'string',
        'status'         => 'integer'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [
        'details',
        'user',
        'job'
    ];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [
//        'amount'
    ];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [
    ];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'user_id'        => 'required',
        'up_latitude'    => 'required',
        'up_longitude'   => 'required',
//        'pick_up'        => 'required',
//        'drop_off'       => 'required',
        'down_latitude'  => 'required',
        'down_longitude' => 'required',
        'status'         => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [

        'user_id'        => 'required',
        'up_latitude'    => 'required',
        'up_longitude'   => 'required',
//        'pick_up'        => 'required',
//        'drop_off'       => 'required',
        'down_latitude'  => 'required',
        'down_longitude' => 'required',
        'status'         => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'user_id'        => 'required',
        'up_latitude'    => 'required',
        'up_longitude'   => 'required',
//        'pick_up'        => 'required',
//        'drop_off'       => 'required',
        'down_latitude'  => 'required',
        'down_longitude' => 'required',
        'status'         => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'user_id'        => 'required',
        'up_latitude'    => 'required',
        'up_longitude'   => 'required',
//        'pick_up'        => 'required',
//        'drop_off'       => 'required',
        'down_latitude'  => 'required',
        'down_longitude' => 'required',
        'status'         => 'required'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function details()
    {
        return $this->hasOne(OrderDetail::class, 'order_id');
    }

    public function job()
    {
        return $this->hasOne(Job::class)->without('order');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function washer()
    {
        return $this->belongsTo(Washer::class);
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

}
